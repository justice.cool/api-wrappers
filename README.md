Language-specific API wrappers for the [Justice.cool API](https://docs.justice.cool/)


# Node.js

Installation:

```bash
npm i @justice.cool/api-wrapper --save
```

Usage: see [samples](./node/samples)


# Your language is not listed ?

You can always use our GraphQL API (see [our documentation](https://docs.justice.cool)).

If you're not happy with GraphQL, contact us to request a wrapper for your language.