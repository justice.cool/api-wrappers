module.exports = {
    client: {
      service: {
          url: "https://api.staging.justice.cool/v1",
      },
      includes: [
          "src/**/*.{ts,tsx,js,jsx,graphql,gql}"
      ]
    },
  };