import { jcoolSchema } from '../gql-types';
import { DocumentNode } from 'apollo-link';

/** Some HTML, translated in your prefered language */
export interface HookHtmlData {
    html: string;
    text: string;
    /** an OPTIONAL property telling which was the original message (if was not in your prefered language) */
    original?: {
        html: string;
        text: string;
        language: string;
    };
}

/** Participant definition */
export interface HookParticipant {
    id: string;
    role: jcoolSchema.AccountRole;
    name: string;
}


/** A hook message that you have received via WebHooks  */
export interface HookPostBody {
    /** An unique identifier for this message. Try to be omnipotent based on this ID */
    hookId: string;
    /** The hook type that must be processed */
    hookName: jcoolSchema.HookName;
    /** The number of times that justice.cool tried to perform this hook against your server */
    retryCount: number;
    /** Some hooks will provide data attached. This is it. */
    data?: any;
    /** @deprecated Prefer using entity.id */
    disputeId?: string;
    /** Which dispute is this hook message related to (justice.cool ID) */
    entity: {
        /** Entity reference */
        id: string;
        /** Entity type */
        type: jcoolSchema.EntityType;
        /** The (optional) external reference provided during dispute creation (if this dispute was created using the API) */
        externalId: string;
    };
    /** Time at which the hook message has been queued */
    time: string | Date;
}

export interface IGqlClient {
    get(query: DocumentNode, variables?: any): Promise<jcoolSchema.JcoolPublicQueries>;
    mutate(query: DocumentNode, variables?: any): Promise<jcoolSchema.JcoolPublicMutations>;
}


export interface IHookManager {

    /**
     * [DEV ONLY !] Uses polling to retreive new hooks to be executed.
     * @deprecated Polling should only be used in development environment, since it is less reslient to errors. Please implement web-hooks in production by calling 'process' method (see our documentation).
     */
    startSimplePolling(): void;

    /** Stops polling events */
    stopPolling(): void;

    /**
     * Processes a hook manually.
     *
     * If you are implementing HTTP POST web-hook mechanism, just pass the request body that has been sent to justice.cool to this function: It will be dispatched to the hooks functions you defined.
     *
     * Returns a promise that must be awaited in the webhook without swallowing the potential error, so justice.cool knows when processing has failed on your server.
     **/
    process(hook: HookPostBody): Promise<void>;
}

/**
 * Data received when a message is posted on a dispute
 *
 * @see https://docs.justice.cool/#/general/hooks?id=message-received
 */
export interface HookMessageData extends HookHtmlData {
    sender?: HookParticipant;
}


/**
 * Data received when someone rejects an invitation
 *
 * @see https://docs.justice.cool/#/general/hooks?id=rejected-invitation
 */
export interface HookRejectionData {
    rejected: HookParticipant;
    /** An optional message that has been provided by the person who rejected the invitation */
    message?: HookHtmlData;
}

/**
 * Data received when a mediation succeeds
 *
 * @see https://docs.justice.cool/#/general/hooks?id=mediation-success
 */
export interface HookMediationSuccessData {
    /** A temporary HTTP link to download the signed contract */
    contract: string;
}


/**
 * Data received when a mediation fails
 *
 * @see https://docs.justice.cool/#/general/hooks?id=mediation-failure
 */
export interface HookMediationFailureData {
    /** A temporary HTTP link to download the proof of mediation failure */
    proofOfMediationFailure: string;
}


/**
 * Data received when you have been invited to a mediation
 *
 * @see https://docs.justice.cool/#/general/hooks?id=invitation-to-a-new-mediation
 */
export interface HookInvitationData {
    /** A for that must be filled (if you do not intend to automate it) */
    invitationForm: string;
}

/** Hooks that you may implement */
export interface IHooks {
    /** Someone filled a new dispute against you */
    newDispute?(info: HookPostBody, invitation: HookInvitationData): void | Promise<void>;

    /** Your opponent has negociated something */
    mediationNegociation?(info: HookPostBody): void | Promise<void>;

    /** Some information is missing */
    missingInfo?(info: HookPostBody): void | Promise<void>;

    /** Dispute has been closed on justice.cool side */
    sleepingDispute?(info: HookPostBody): void | Promise<void>;

    /** An invitation has been rejected */
    invitationRejection?(info: HookPostBody, data: HookRejectionData): void | Promise<void>;

    /** A contract requires your signature */
    requiresSignature?(info: HookPostBody): void | Promise<void>;

    /** Your opponent has logged-in for the first time on this dispute */
    opponentLoggedIn?(info: HookPostBody): void | Promise<void>;

    /** End of mediation */
    mediationSuccess?(info: HookPostBody, data: HookMediationSuccessData): void | Promise<void>;

    /** End of mediation */
    mediationFailure?(info: HookPostBody, data: HookMediationFailureData): void | Promise<void>;

    /** New message received */
    messageReceived?(dispute: HookPostBody, message: HookMessageData): void | Promise<void>;

    /** Your opponent has rejected the invitation to join */
    invitationRejection?(dispute: HookPostBody): void | Promise<void>;

    /** The court clerk has been invited in your file */
    courtClerkInvited?(dispute: HookPostBody): void | Promise<void>;

    /** Conclusions have been uploaded */
    conclusionsUploaded?(dispute: HookPostBody): void | Promise<void>;

    /** The decision has been uploaded */
    decisionUploaded?(dispute: HookPostBody): void | Promise<void>;

    /** Someone invited you on to take part to a lawsuit */
    newLawsuit?(dispute: HookPostBody): void | Promise<void>;

    /** A lawsuit date changed */
    lawsuitDateChange?(dispute: HookPostBody): void | Promise<void>;

    /** A transaction/decision has been executed */
    execution?(dispute: HookPostBody): void | Promise<void>;

    /** A transaction/decision has been settled */
    agreement?(dispute: HookPostBody): void | Promise<void>;

}

export type DisputeSimpleDetails = Pick<jcoolSchema.Dispute, 'id' | 'externalId' | 'currentFormLink' | 'link' | 'myRole' | 'creationDate' | 'participants' | 'countDownDate' | 'step' | 'newsfeedEmail'>;

export type Message = Pick<jcoolSchema.Newsfeed, 'category' | 'channel' | 'date'>
    & {
        content: jcoolSchema.NewsfeedMessageData
    };

export type Fact = Pick<jcoolSchema.DisputeFact, 'ref' | 'name' | 'participant' | 'value' | 'valueType' | 'status'>;
export type Claim = Pick<jcoolSchema.Claim, 'id' | 'status' | 'name' | 'type' | 'participant' | 'myLastProposition' | 'initialProposition' | 'lastProposition'>;
