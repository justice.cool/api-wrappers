import { ApolloClient, DocumentNode, InMemoryCache } from 'apollo-boost';
import { createHttpLink } from 'apollo-link-http';
import fetch from 'node-fetch';
import { setContext } from 'apollo-link-context';
import { jcoolSchema } from '../gql-types';
import { IGqlClient } from '.';

export class GqlClient implements IGqlClient {

    private client: ApolloClient<any>;
    private token: string;

    constructor(endpoint: string, apiKey: string) {
        this.token = apiKey;

        // http link
        const httpLink = createHttpLink({
            uri: endpoint,
            fetch: <any>fetch,
        });

        // auth middleware (injects token header)
        const authLink = setContext((_, { headers }) => {
            return {
                headers: {
                    ...headers,
                    authorization: this.token ? `Bearer ${this.token}` : '',
                }
            }
        });

        // appollo client
        this.client  = new ApolloClient({
            link: authLink.concat(httpLink),
            cache: new InMemoryCache(),
            defaultOptions: {
                watchQuery: {
                  fetchPolicy: 'no-cache',
                  errorPolicy: 'ignore',
                },
                query: {
                  fetchPolicy: 'no-cache',
                  errorPolicy: 'all',
                },
            },
        });
    }

    setToken(who: string) {
        this.token = who;
    }


    async get(query: DocumentNode, variables?: any): Promise<jcoolSchema.JcoolPublicQueries> {
        const ret = await this.client.query({
            query: query,
            variables: variables,
        }).catch(e => this.formatError(e));
        return ret.data;
    }

    async mutate(query: DocumentNode, variables?: any): Promise<jcoolSchema.JcoolPublicMutations> {
        const ret = await this.client.mutate({
            mutation: query,
            variables: variables,
        }).catch(e => this.formatError(e));
        return ret.data;
    }

    private formatError(e: any): any {
        // just concats the graphql error and rethrows them
        const message = e.graphQLErrors.map (x => `(${x.code}) ${x.error}`).join('\n');
        throw new Error(message);
    }
}