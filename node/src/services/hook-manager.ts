import { IHookManager, IGqlClient, HookPostBody, IHooks } from '.';
import gql from 'graphql-tag';
import { jcoolSchema } from '../gql-types';
import { assertUnreachable } from '../utils';



export class HookManager implements IHookManager {

    private started: boolean;
    private timeOut: NodeJS.Timeout;
    private _hooks: IHooks;

    private get hooks(): IHooks {
        if (this._hooks) {
            return this._hooks;
        }
        if (typeof this._hooksGetter === 'function') {
            this._hooks = this._hooksGetter(this.api);
        } else {
            this._hooks = this._hooksGetter;
        }
        return this._hooks;
    }

    constructor(private client: IGqlClient, private api: any, private _hooksGetter: IHooks | ((api?: any) => IHooks)) {
    }

    startSimplePolling(): void {
        if (this.started) {
            return;
        }
        this.timeOut = setTimeout(() => {
            this.doPoll();
        }, 5000);
        this.started = true;
    }

    stopPolling(): void {
        this.started = false;
        clearTimeout(this.timeOut);
    }


    async doPoll() {
        if (!this.started)
            return;
        try {
            // poll hook messages
            const { hooks } = await this.client.get(gql`query FetchHooks {
                hooks {
                     list {
                         entity {
                            ...on IEntitySummary {
                              id
                              type
                              externalId
                            }
                         }
                         hookId
                         hookName
                         data
                         time
                         retryCount
                     }
                }
            }`);

            if (!hooks.list.length) {
                return;
            }

            const oks: string[] = [];
            const errors: jcoolSchema.HookErrorInput[] = [];
            // process messages
            for (const h of hooks.list) {
                if (!this.started) {
                    return;
                }

                // process hook
                try {
                    await this.process({
                        disputeId: h.entity.id,
                        entity: {
                            id: h.entity.id,
                            type: h.entity.type,
                            externalId: h.entity.externalId,
                        },
                        data: h.data,
                        hookName: h.hookName,
                        hookId: h.hookId,
                        retryCount: h.retryCount,
                        time: new Date(h.time)
                    });
                    oks.push(h.hookId);
                } catch (e) {
                    errors.push({
                        hookId: h.hookId,
                        error: e.toString(),
                    });
                }
            }

            if (!this.started) {
                return;
            }

            // once they have been processed, mark them so they will not get re-processed.
            await this.client.mutate(gql`mutation MarkHookProcessed($oks: [String!]!, $errors: [HookErrorInput!]!) {
                hooks {
                    markProcessed(hookIds: $oks)
                    markErrored(hooks: $errors)
                }
            }`, {
                oks,
                errors,
            });

        } catch (e) {
            console.error('Failed to poll justice.cool hooks', e);
        } finally {
            if (!this.started) {
                return;
            }
            // re-schedule polling
            this.timeOut = setTimeout(() => this.doPoll(), 5000);
        }
    }

    private coerce<T extends Function>(fn: T): T {
        return <T>(fn || function () { }).bind(this.hooks);
    }
    async process(hook: HookPostBody): Promise<void> {
        switch (hook.hookName) {
            case jcoolSchema.HookName.Message:
                return await this.coerce(this.hooks.messageReceived)(hook, hook.data);
            case jcoolSchema.HookName.NewDispute:
                return await this.coerce(this.hooks.newDispute)(hook, hook.data);
            case jcoolSchema.HookName.MediationSuccess:
                return await this.coerce(this.hooks.mediationSuccess)(hook, hook.data);
            case jcoolSchema.HookName.MediationFailure:
                return await this.coerce(this.hooks.mediationFailure)(hook, hook.data);
            case jcoolSchema.HookName.MediationNegociation:
                return await this.coerce(this.hooks.mediationNegociation)(hook);
            case jcoolSchema.HookName.OpponentLoggedIn:
                return await this.coerce(this.hooks.opponentLoggedIn)(hook);
            case jcoolSchema.HookName.RequiresSignature:
                return await this.coerce(this.hooks.requiresSignature)(hook);
            case jcoolSchema.HookName.SleepingDispute:
                return await this.coerce(this.hooks.sleepingDispute)(hook);
            case jcoolSchema.HookName.InvitationRejection:
                return await this.coerce(this.hooks.invitationRejection)(hook, hook.data);
            case jcoolSchema.HookName.CourtClerkInvited:
                return await this.coerce(this.hooks.courtClerkInvited)(hook);
            case jcoolSchema.HookName.ConclusionsUploaded:
                return await this.coerce(this.hooks.conclusionsUploaded)(hook);
            case jcoolSchema.HookName.DecisionUploaded:
                return await this.coerce(this.hooks.decisionUploaded)(hook);
            case jcoolSchema.HookName.NewLawsuit:
                return await this.coerce(this.hooks.newLawsuit)(hook);
            case jcoolSchema.HookName.LawsuitDateChange:
                return await this.coerce(this.hooks.lawsuitDateChange)(hook);
            case jcoolSchema.HookName.Execution:
                return await this.coerce(this.hooks.execution)(hook);
            case jcoolSchema.HookName.Agreement:
                return await this.coerce(this.hooks.agreement)(hook);
            default:
                assertUnreachable(hook.hookName);
        }
    }
}