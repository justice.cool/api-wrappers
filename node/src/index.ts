import {GqlClient} from './services/gql-client';
import { IHookManager, IGqlClient, IHooks } from './services';
import { HookManager } from './services/hook-manager';
import { jcoolSchema } from './gql-types';
import gql from 'graphql-tag';
import { DisputeManager } from './dispute';
export * from './services';
export * from './gql-types';

export enum Env {
    prod = 'https://api.justice.cool/v1',
    staging = 'https://api.staging.justice.cool/v1',
}

/** Justice.cool api wrapper helper. Use ".client" property for custom GraphQL requests. */
export class JCoolApi {

    client: IGqlClient;
    hooks: IHookManager;

    constructor(apiKey: string, hooks: IHooks | ((api?: JCoolApi) => IHooks), apiUrl?: Env) {
        this.client = new GqlClient(apiUrl, apiKey);
        this.hooks = new HookManager(this.client, this, hooks);
    }

    /** Creates a new dispute, and returns its Justice.cool ID */
    async createDispute(dispute: jcoolSchema.DisputeCreationInput): Promise<string> {
        const {createDispute} = await this.client.mutate(gql`mutation CreateDispute($dispute: DisputeCreationInput!) {
            createDispute (data: $dispute) {
                id
            }
        }`, {
            dispute
        });
        return createDispute.id;
    }

    /** Get a dispute manager */
    getDispute(dispute: string): DisputeManager {
        return new DisputeManager(dispute, this.client);
    }

    /** Get the last disputes you are involved in */
    async getDisputes() {
        const {disputes} = await this.client.get(gql`query GetDisputes {
            disputes {
                id
                externalId
                step {
                    id
                    categoryId
                    title
                }
                countDownDate
                currentFormLink
                link
                newsfeedEmail
            }
        }`);

        return disputes;
    }
}