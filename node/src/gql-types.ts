// ========= THIS IS AN AUTOMATICALLY GENERATED FILE, DO NOT EDIT IT ! ============
export namespace jcoolSchema {
  export type Maybe<T> = T | null;

  export interface DisputeFilterInput {
    /** Uses OR insteand of AND for inclusion fields (which then become mutualy inclusive... building up a bigger selection) */
    inclusive?: Maybe<boolean>;
    /** [inclusive field] Selects disputes with these macro steps */
    statuses?: Maybe<string[]>;
    /** [exclusive field] Filter disputes by id, name or participant */
    suggest?: Maybe<string>;
    /** Entity id from which to query. */
    refEntity?: Maybe<string>;
    /** What data to use to order */
    orderBy?: Maybe<DisputeColumn>;
    /** True if ascending order. */
    ascending?: Maybe<boolean>;
    /** [exclusive field] Only selects results on a specific namespace (ex: "xxx" for disputes of xxx.on.justice.cool) */
    namespace?: Maybe<string>;
    /** [exclusive field] Only selects results with a specific court type (ex: commercial) */
    court?: Maybe<string>;
    /** [exclusive field] Removes these statuses from the selection set */
    exceptStatuses?: Maybe<string[]>;
    /** [exclusive field] Removes these groups from the selection set */
    exceptGroups?: Maybe<string[]>;
    /** Returns only lawsuits that have had this number of hearings (if "5" is included, then returns lawsuits with more than 5 hearings) */
    hearingsCounts?: Maybe<(Maybe<number>)[]>;
    /** [inclusion field] Selects specific dispute IDs */
    disputes?: Maybe<string[]>;
    /** [exclusive field] Removes these dispute IDs from the selection set */
    exceptDisputes?: Maybe<string[]>;
    /** [inclusion field] Selects disputes that are included in these lawsuits */
    inLawsuits?: Maybe<string[]>;
    /** [inclusion field] Selects disputes that are included in these lawsuits */
    inLawsuitsFilter?: Maybe<LawsuitFilterInput>;
    /** Only selects results concerning specific laws (see Dispute.eligible_laws column) */
    withLaws?: Maybe<string[]>;
    /** [inclusion field] Filter on this group */
    group?: Maybe<string>;
    /** [inclusion field] Filter on these groups */
    groups?: Maybe<string[]>;
  }

  export interface LawsuitFilterInput {
    /** Uses OR insteand of AND for inclusion fields (which then become mutualy inclusive... building up a bigger selection) */
    inclusive?: Maybe<boolean>;
    /** [inclusive field] Selects disputes with these macro steps */
    statuses?: Maybe<string[]>;
    /** [exclusive field] Filter disputes by id, name or participant */
    suggest?: Maybe<string>;
    /** Entity id from which to query. */
    refEntity?: Maybe<string>;
    /** What data to use to order */
    orderBy?: Maybe<LawsuitColumn>;
    /** True if ascending order. */
    ascending?: Maybe<boolean>;
    /** [exclusive field] Only selects results on a specific namespace (ex: "xxx" for disputes of xxx.on.justice.cool) */
    namespace?: Maybe<string>;
    /** [exclusive field] Only selects results with a specific court type (ex: commercial) */
    court?: Maybe<string>;
    /** [exclusive field] Removes these statuses from the selection set */
    exceptStatuses?: Maybe<string[]>;
    /** [exclusive field] Removes these groups from the selection set */
    exceptGroups?: Maybe<string[]>;
    /** Returns only lawsuits that have had this number of hearings (if "5" is included, then returns lawsuits with more than 5 hearings) */
    hearingsCounts?: Maybe<(Maybe<number>)[]>;
    /** [inclusion field] Selects specific lawsuits IDs */
    lawsuits?: Maybe<(Maybe<string>)[]>;
    /** [exclusive field] Removes these lawsuits IDs from the selection set */
    exceptLawsuits?: Maybe<(Maybe<string>)[]>;
    /** Returns lawsuit with transaction in these statuses */
    transactionStatuses?: Maybe<(Maybe<string>)[]>;
    /** [inclusion field] Filter on this group */
    group?: Maybe<string>;
    /** [inclusion field] Filter on these groups */
    groups?: Maybe<string[]>;
  }
  /** Rich HTML input, which will cleaned be translated to other languages, and with optional attached documents */
  export interface RichHtmlInput {
    /** An HTML string Justice.cool will clean this html (removes all styles, scripts, ...) to make it safe to display. */
    html: string;
    /** Document references (see our documentation to know how to upload them), or http links */
    documents?: Maybe<string[]>;
  }
  /** justice.cool existing variable, its answer, and the corresponding participant if required. */
  export interface FactChangeInput {
    /** The existing variable ID (see https://docs.justice.cool/#/known-variables to learn about existing variables) */
    variable: string;
    /** The answer to the given question */
    answer: Json;
    /** If the fact being provided is a "personal" fact, you will be expected to provide participant ID. */
    participantId?: Maybe<string>;
    /** An optional comment that you may provide to justify this fact */
    comment?: Maybe<RichHtmlInput>;
  }
  /** A combination of both an ID and a participant ID */
  export interface IdPidInput {
    /** Variable ID */
    id: string;
    /** If the variable you are refering to is a personal variable, and if you have multiple demanders, this must be filled with the participant ID you are targetting */
    pid?: Maybe<string>;
  }
  /** Your own version about a fact, or a rejection of a fact value */
  export interface FactAmendmentInput {
    /** The fact reference to be amended */
    ref: string;
    /** The value you would like to be fixed. - If you would like to give your own version of this fact, then provide a value. Refer to https://docs.justice.cool/#/known-variables to know which kind of data we might be expecting, which depends on the type of fact you are reviewing. Most of the fact types (numbers, dates) are quite straightforward (ISO json formats are accepted). Contact us if you have doubts about how to format complex types (date ranges, ...) - If you would like to signify your opponent that you reject this value (i.e. to tell your opponent "please correct the value you provided"), then omit this value (or input "null"). NB: You can only reject values for facts that have originally been answered by your opponent. Not you. */
    value?: Maybe<Json>;
    /** An optional comment about this version */
    comment?: Maybe<RichHtmlInput>;
  }
  /** A counter-proposal to a claim proposal */
  export interface ClaimCounterPropositionInput {
    /** The claim ID for which you want to post a counter-proposal. */
    claimId: string;
    /** [Only for money claims] Make a counter proposal */
    compensation?: Maybe<ClaimCompensationInput>;
    /** [Only for service claims] What is asked */
    service?: Maybe<ClaimServiceInput>;
    /** A comment to explain your choice */
    comment?: Maybe<RichHtmlInput>;
  }
  /** Claim compensation (money). You can either provide a formulat to compute how much will be due (the amount will be updated based on the negociation of facts), or give a fixed amount. */
  export interface ClaimCompensationInput {
    /** A fixed amount for this compensation */
    amount: number;
    /** A formulat that can be used to compute this compensation from given variables (see documentation) */
    formula?: Maybe<string>;
    /** The corresponding curency */
    currency?: Currency;
  }
  /** Claim service description */
  export interface ClaimServiceInput {
    /** How much time (in days) is given to deliver the service, starting after dispute resolution */
    delay: number;
    /** Optional description/details of the service */
    description?: Maybe<string>;
  }
  /** A claim rejection: This invites your opponent to make a counter-proposal */
  export interface ClaimRejectionInput {
    /** The claim ID for which you want to post a counter-proposal. */
    claimId: string;
    /** A comment to explain your choice */
    comment?: Maybe<RichHtmlInput>;
  }
  /** Input data to create a new dispute */
  export interface DisputeCreationInput {
    /** Choses which optional justice.cool functionalities you want to use for this dispute. */
    features: JCoolFeature[];
    /** You can choose what is the initial step of this dispute, depending on your needs (defaults to "mediation") */
    startAt?: JcoolEntry;
    /** The lawyer you would like to invite to help you */
    lawyer?: Maybe<LawyerLinkInput>;
    /** The organization you would like to invite to help you */
    organization?: Maybe<OrgLinkInput>;
    /** Notes you want to add for you, which will be visible later on justice.cool (HTML format). nb1: These notes are only visible by you. nb2: You can add hashtags in notes to help you classify, for instance "#some-tag" */
    notes?: Maybe<string>;
    /** (⚠ ADVANCED) Specifying it allows you to create a dispute directly handled by one lawyer that has created an on.justice.cool space */
    namespace?: Maybe<DisputeCreationNamespace>;
    /** This tells justice.cool how you would like to sign documents: When we contact the opponent, we will provide him: - A PDF document containing all the files you gave us - A summary of all the facts (variables) you gave us - The list of all your claims - A pre-signed contract stating that you will abandon all pursuits if they execute all your claims as it (without negociation) In order to send this contract, we need you to sign it. Moreover, if the opponent starts a negociation, and the mediation succeeds, justice.cool will need to generate a new contract, that you will have to sign. See documentations of available options. */
    signatureMode: SignatureMode;
    /** (optional) An ID to identify this dispute in your systems (neutral for justice.cool, this just makes support easier) */
    externalId?: Maybe<string>;
    /** Tells justice.cool how it should handle the data you provided (see UpdateMode description) */
    updateMode: UpdateMode;
    /** Variables which are not tied to a participant, and their values to fill-in the dispute (see VariableDataInput description). If 'mode' is not 'manual', those variables will be used to automatically assess what the demander is entitled to. */
    facts?: Maybe<FactDataInput[]>;
    /** Custom variables which are not tied to a participant (i.e. variables that are unknown to justice.cool) */
    customFacts?: Maybe<CustomFactDataInput[]>;
    /** The list of demanders (physical persons involved in the dispute), and their claims. If you omit this field, then Justice.cool will consider that your company is the demander. */
    demanders?: Maybe<DisputeDemanderInput[]>;
    /** In case you specify demanders, this role will be given to you and demanders will be invited to create an account to join the dispute */
    myRole?: Maybe<AccountRole>;
    /** Who is your opponent */
    opponent?: Maybe<OpponentInput>;
    /** A list of custom documents that will be attached to this dispute. nb: Those are different from the documents required by our modelization. */
    otherDocuments?: Maybe<OtherDocumentInput[]>;
  }

  export interface LawyerLinkInput {
    /** The lawyer identifier to invite */
    id: string;
    /** The lawyer fee agreement that you have signed with this lawyer */
    lawyerFeeAgreement?: Maybe<string>;
    /** Notes you want to add for your lawyer (HTML format). nb1: Only your lawyer will see those notes. You wont, nor your opponent will. nb2: You can add hashtags in notes to help you classify, for instance "#some-tag" */
    notes?: Maybe<string>;
  }

  export interface OrgLinkInput {
    /** The organization identifier to invite */
    id: string;
    /** Specify whether it is an association or a legal protection */
    role: AccountRole;
    /** Notes you want to add for your organization (HTML format). nb1: Only your organization will see those notes. You wont, nor your opponent will. nb2: You can add hashtags in notes to help you classify, for instance "#some-tag" */
    notes?: Maybe<string>;
  }

  export interface DisputeCreationNamespace {
    /** The lawyer technical ID */
    id: string;
    /** The lawyer model to use */
    model: string;
  }
  /** justice.cool existing variable, and its answer */
  export interface FactDataInput {
    /** The existing variable ID (see https://docs.justice.cool/#/known-variables to learn about existing variables) */
    variable: string;
    /** The answer to the given question */
    answer: Json;
  }
  /** justice.cool existing fact, and its answer */
  export interface CustomFactDataInput {
    /** An ID for this fact (which you can use in formulas, or which could be useful to you to update this information later) */
    id: string;
    /** The name of this data (will be shown as title) */
    name: string;
    /** (optional) Some details to describe this data */
    description?: Maybe<string>;
    /** Type of data */
    type?: Maybe<CustomVariableType>;
    /** The answer to the given question */
    answer: Json;
  }
  /** Information about a person or company involved in this dispute as demander, his claims, and the facts related to this demander. If someone or a company has mandated your company to handle this dispute, then you MUST fill either 'person' or 'company' fields. If neither are filled, or if you have ommited 'demanders', then Justice.cool will consider that your company is the demander. */
  export interface DisputeDemanderInput {
    /** When the demander is a person that mandated your company, fill this information. */
    person?: Maybe<DemanderPersonInput>;
    /** When the demander is a company that mandated your company, fill this information */
    company?: Maybe<CompanyInput>;
    /** (optional) An ID to identify this demander in your systems (neutral for justice.cool, this just makes support easier) */
    externalId?: Maybe<string>;
    /** Variables specific to this participant, and their values to fill-in the dispute (see VariableDataInput description). If 'mode' is not 'manual', those variables will be used to automatically assess what the demander is entitled to. */
    facts?: Maybe<FactDataInput[]>;
    /** Custom variables specific to this participants (i.e. variables that are unknown to justice.cool) */
    customFacts?: Maybe<CustomFactDataInput[]>;
    /** Claims to create manually. If creating "manual" dispute, then this must be filled. For "auto" disputes, claims will be created automatically. In which case, however, you can create additional claims or force some claims amounts. See ClaimCreationInput.eligibilityMode and ClaimCreationInput.compensationMode to know more about how those claims will be handled. */
    claims?: Maybe<ClaimCreationInput[]>;
  }
  /** Information about a demander (when is a person) */
  export interface DemanderPersonInput {
    /** First name of the demander (ex: John) */
    firstName: string;
    /** Last name of the demander (ex: Smith) */
    lastName: string;
    /** Postal address of the demander. It is not required during the mediation phase, but it will be asked later if not provided if you plan to use Justice.cool during the judiciary process. If you know it already, provide it as soon as possible. */
    address?: Maybe<PostalAddressInput>;
    /** Demander email address (optional - might be useful to export your users email address to justice.cool if you plan to give some access to your lawyer) */
    email?: Maybe<string>;
    /** Demander phone (optional - might be useful to export your users phone number to justice.cool if you plan to give some access to your lawyer) */
    phone?: Maybe<string>;
    /** The language used by the demander (French by default) */
    language?: Maybe<Language>;
    /** Demander's birth date. It is not required during the mediation phase, but it will be asked later if not provided if you plan to use Justice.cool during the judiciary process. If you know it already, provide it as soon as possible. */
    birthDate?: Maybe<string>;
    /** Demander's city of birth (ex: Paris). It is not required during the mediation phase, but it will be asked later if not provided if you plan to use Justice.cool during the judiciary process. If you know it already, provide it as soon as possible. */
    birthPlace?: Maybe<string>;
    /** Demander's profession. It is not required during the mediation phase, but it will be asked later if not provided if you plan to use Justice.cool during the judiciary process. If you know it already, provide it as soon as possible. */
    profession?: Maybe<string>;
    /** Demander's nationality (ex: FRA). ⚠ This field expects an ISO 3166-1 alpha-3 country code (https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3). However, it will try to infer it if you provide a value like "France", "Francaise" or "French". It is not required during the mediation phase, but it will be asked later if not provided if you plan to use Justice.cool during the judiciary process. If you know it already, provide it as soon as possible. */
    nationality?: Maybe<string>;
    /** Demander's gender */
    gender?: Maybe<Gender>;
  }
  /** A postal address (without recipent name) */
  export interface PostalAddressInput {
    /** First line of address (ex: 5 parvis alan turing) */
    line1: string;
    /** (optional) second line of address - if required */
    line2?: Maybe<string>;
    /** Postal/Zip code (ex: 75013) */
    postalCode: string;
    /** City name (ex: Paris) */
    city: string;

    country: string;
  }
  /** Identifies a company. */
  export interface CompanyInput {
    /** You can explicitely specifiy the company name here, as its usual name (it will be used to identify this company in our interface and in juridic documents). */
    name?: Maybe<string>;
    /** Company identifier. See our documentation at https://docs.justice.cool. */
    identifier: string;
    /** Language used by this company */
    language?: Maybe<Language>;
    /** Postal address of the demander. It is not required during the mediation phase, but it will be asked later if not provided if you plan to use Justice.cool during the judiciary process. If you know it already, provide it as soon as possible. */
    address?: Maybe<PostalAddressInput>;
    /** Email address of the demander. Required if you are not the demander, nor mandated by him/her */
    contactEmail?: Maybe<string>;
  }
  /** Input data for a claim */
  export interface ClaimCreationInput {
    /** (optional for manual claims) Specify this if you want your claim to match a claim type that is known to justice.cool (so it can be automatically scored, and updated) */
    typeId?: Maybe<string>;
    /** (mandatory for custom claims) The name of the claim to be created (will be shown to opponent) */
    name?: Maybe<string>;
    /** (optional) A formula that will be used to compute if whether if this claim is eligible (it will be deleted if this expression is false after facts negociation). See formula documentation. */
    eligibility?: Maybe<string>;
    /** (optional) An ID to identify this claim in your systems (neutral for justice.cool, this just makes support easier) */
    externalId?: Maybe<string>;
    /** [Only for non "manual" disputes] Tells justice.cool how to update eligibility of this claim as users negociate related information. Giving something else than 'manual' could delete this claim automatically (if justice.cool detects it is not eligible anymore after opponent reviewed facts) */
    eligibilityMode?: UpdateMode;
    /** [Only for money claims, in non "manual" disputes] Tells justice.cool how to update claim compensation (i.e. amount to pay) */
    compensationMode?: UpdateMode;
    /** [Only for money claims] How much is asked */
    compensation?: Maybe<ClaimCompensationInput>;
    /** [Only for service claims] What is asked */
    service?: Maybe<ClaimServiceInput>;
  }

  export interface OpponentInput {
    /** Your opponent is a person ? Fill this. */
    person?: Maybe<PersonOpponentInput>;
    /** Your opponent is a company ? Fill this. */
    company?: Maybe<CompanyInput>;
    /** If your opponent is using a CRM and you already have contacted them about this dispute, then it is likely that they have given you an identifier to specify in all email exchanges that you have with them. You can give us this identfier here: Justice.cool will append this (or these) identifier(s) to the subject of every mail that is sent to them, so their CRM can keep track of your dispute. */
    crmReferences?: Maybe<(Maybe<string>)[]>;
    /** Tells Justice.cool which language to use when contacting this opponent. If you do not provide this, depending on the kind of opponent, our euristics will try to determine which language to use automatically. But you might want to force this value if you already know which language to use. */
    language?: Maybe<Language>;
    /** Tell us how you would like your opponent to be contacted. If not provided, then we will try to find the best way to contact them for you (only works for companies). WARNING: If you are using Justice.cool for mediation platform, you MUST specify a postal address with "premium" as contact mode. This is the only way you will be able to produce a reliable proof of contact in front of a tribunal. Not providing it will fail. This is however not a requirement when using Justice.cool as a procedure or negociation platform. */
    contactMeans?: Maybe<ContactMeanInput[]>;
  }
  /** Your opponent is a person */
  export interface PersonOpponentInput {
    gender?: Maybe<Gender>;

    firstName?: Maybe<string>;

    lastName: string;
  }
  /** A contact mean. Specify only one of the properties + "mode". Technical NB: This will be replaced by a "union input type" as soon as GraphQL supports them. This type is in fact: {address: Address} | {email: String} | {phone: String} */
  export interface ContactMeanInput {
    /** [ONLY FOR COMPANIES] Setting this to true is the same as not specifying "contactMeans":  determine how to contact your opponent for you. But using this, you are given the chance add additional alternative contact modes */
    auto?: Maybe<boolean>;
    /** Contact sending a postal letter  to this address */
    address?: Maybe<PostalAddressInput>;
    /** justice.cool will send an SMS to this address */
    phone?: Maybe<string>;
    /** justice.cool will send an email to this address */
    email?: Maybe<string>;
  }

  export interface OtherDocumentInput {
    /** A name to attach to this document. nb: If you wish to provide multiple translations of this name, you can provide a JSON input like this instead of a simple string:  {"fr": "Nom du document", "en":"Document name"} */
    name?: Maybe<LocalizedString>;
    /** Document to upload. You can provide an URL (if the document is reasonably heavy), or use one of the methods described here to upload your document and give us its ID: https://docs.justice.cool/#/general/documents */
    value: string;
    /** Describes who can see this document (defaults to "everybody") */
    access?: Maybe<SimpleAccess>;
  }
  /** Some details about your hook error (you will find them in the "developer" tab on the corresponding disputes) */
  export interface HookErrorInput {
    /** The hook ID */
    hookId: string;
    /** Some error message */
    code?: Maybe<number>;
    /** Some error message */
    error?: Maybe<string>;
  }
  /** Describes which hooks to retreive */
  export enum HookFilter {
    Polling = "polling",
    NonProcessed = "nonProcessed",
    All = "all"
  }
  /** Enumeration of all the possible hook events that might occur */
  export enum HookName {
    MediationSuccess = "mediationSuccess",
    MediationFailure = "mediationFailure",
    Message = "message",
    MediationNegociation = "mediationNegociation",
    RequiresSignature = "requiresSignature",
    NewDispute = "newDispute",
    SleepingDispute = "sleepingDispute",
    OpponentLoggedIn = "opponentLoggedIn",
    InvitationRejection = "invitationRejection",
    CourtClerkInvited = "courtClerkInvited",
    ConclusionsUploaded = "conclusionsUploaded",
    DecisionUploaded = "decisionUploaded",
    NewLawsuit = "newLawsuit",
    LawsuitDateChange = "lawsuitDateChange",
    Execution = "execution",
    Agreement = "agreement"
  }

  export enum EntityType {
    Lawsuit = "lawsuit",
    Conciliation = "conciliation",
    Dispute = "dispute"
  }
  /** Role of a participant to a dispute */
  export enum AccountRole {
    Demander = "demander",
    Defender = "defender",
    DemanderLawyer = "demanderLawyer",
    DefenderLawyer = "defenderLawyer",
    DemanderBailiff = "demanderBailiff",
    DemanderLawyerSurrogate = "demanderLawyerSurrogate",
    DefenderLawyerSurrogate = "defenderLawyerSurrogate",
    DemanderAccountant = "demanderAccountant",
    DemanderLegalProtection = "demanderLegalProtection",
    Association = "association",
    CourtClerk = "courtClerk",
    Creditor = "creditor"
  }

  export enum Gender {
    Male = "male",
    Female = "female"
  }
  /** Side of a dispute */
  export enum DisputeSide {
    Demander = "demander",
    Defender = "defender"
  }
  /** Defines how you would like contracts to be signed */
  export enum SignatureMode {
    Auto = "auto",
    SemiAuto = "semiAuto",
    Manual = "manual"
  }

  export enum JCoolFeature {
    LetMeCheckBeforeSending = "letMeCheckBeforeSending",
    IWillUploadDocumentsLater = "iWillUploadDocumentsLater",
    SignContractManually = "signContractManually"
  }

  export enum UpdateMode {
    Manual = "manual",
    Try = "try",
    Auto = "auto"
  }

  export enum DisputeColumn {
    CreationDate = "creationDate",
    LastUpdate = "lastUpdate"
  }

  export enum LawsuitColumn {
    CourtFileNumber = "courtFileNumber",
    CreationDate = "creationDate",
    LastUpdate = "lastUpdate"
  }
  /** All actions that can be performed on a dispute */
  export enum DisputeAction {
    ForceResolve = "forceResolve",
    ModifyFacts = "modifyFacts",
    ModifyParticipants = "modifyParticipants",
    AcceptInvitation = "acceptInvitation",
    CustomizeOpponentContact = "customizeOpponentContact",
    SetDisputeAsVerified = "setDisputeAsVerified",
    SendFormalNotice = "sendFormalNotice",
    SendGuidedFormalNotice = "sendGuidedFormalNotice",
    SendComplaint = "sendComplaint",
    SendGuidedComplaint = "sendGuidedComplaint",
    AcceptConciliation = "acceptConciliation",
    RefuseConciliation = "refuseConciliation",
    StartConciliation = "startConciliation",
    LawyerFileCompleted = "lawyerFileCompleted",
    CheckedDisputeOk = "checkedDisputeOk",
    CancelDispute = "cancelDispute",
    DefenderPaymentIntention = "defenderPaymentIntention",
    DefenderPaid = "defenderPaid",
    NotifyDisputeResolved = "notifyDisputeResolved",
    ClientRefunded = "clientRefunded",
    BankDetailsFilled = "bankDetailsFilled",
    AskDetails = "askDetails",
    ModifyClaims = "modifyClaims",
    MediationFailedChoice = "mediationFailedChoice",
    OrgFileEndChoice = "orgFileEndChoice",
    Prosecute = "prosecute",
    SetCourt = "setCourt",
    StartJudiciaryProcess = "startJudiciaryProcess",
    StartLawyerDispute = "startLawyerDispute",
    InviteDemanderLawyer = "inviteDemanderLawyer",
    InviteLegalProtection = "inviteLegalProtection",
    ChooseConciliationLawyer = "chooseConciliationLawyer",
    InviteDemanderAccountant = "inviteDemanderAccountant",
    SkipFeeAgreement = "skipFeeAgreement",
    ManageLawyerFees = "manageLawyerFees",
    ExtendMediation = "extendMediation",
    EnterMediation = "enterMediation",
    StartMediation = "startMediation",
    UploadProofOfDeposit = "uploadProofOfDeposit",
    UploadComplaintResponse = "uploadComplaintResponse",
    UploadFeeAgreement = "uploadFeeAgreement"
  }
  /** All actions that can be performed on a lawsuit */
  export enum LawsuitAction {
    RedactSubpoena = "redactSubpoena",
    SetSubpoenaAsSent = "setSubpoenaAsSent",
    EditSubpoena = "editSubpoena",
    EditSubpoenaProject = "editSubpoenaProject",
    EditPetition = "editPetition",
    CloseLawsuit = "closeLawsuit",
    AbortLawsuit = "abortLawsuit",
    DeleteLawsuit = "deleteLawsuit",
    InviteBailiff = "inviteBailiff",
    InviteSurrogateLawyer = "inviteSurrogateLawyer",
    InviteDefenderLawyer = "inviteDefenderLawyer",
    PlacedToCourt = "placedToCourt",
    ServeSubpoena = "serveSubpoena",
    SkipBailiff = "skipBailiff",
    SubpoenaReadyToBeServed = "subpoenaReadyToBeServed",
    UploadOpponentConclusions = "uploadOpponentConclusions",
    PostponeHearingDate = "postponeHearingDate",
    UploadMyConclusions = "uploadMyConclusions",
    SetTransaction = "setTransaction",
    SetTransactionAsExecuted = "setTransactionAsExecuted",
    MakeTransactionProposal = "makeTransactionProposal",
    GiveDecisionDate = "giveDecisionDate",
    UploadDecision = "uploadDecision",
    GenerateDecision = "generateDecision",
    SaveDecision = "saveDecision",
    SetDecisionAsExecuted = "setDecisionAsExecuted",
    RemoveDisputes = "removeDisputes",
    DeleteSubpoena = "deleteSubpoena",
    PostponeDateOfService = "postponeDateOfService",
    PrepareSubpoena = "prepareSubpoena",
    RemoveBailiff = "removeBailiff",
    FillCourtFileNumber = "fillCourtFileNumber",
    GiveFirstHearingDate = "giveFirstHearingDate",
    SentToCourt = "sentToCourt",
    GiveConciliationDate = "giveConciliationDate",
    UploadConciliationReport = "uploadConciliationReport",
    RedactPetitionCerfa = "redactPetitionCerfa",
    SendDocumentsToDefender = "sendDocumentsToDefender",
    ServiceByBailiffNeeded = "serviceByBailiffNeeded",
    GiveLawsuitTimeline = "giveLawsuitTimeline",
    SetSuccessFees = "setSuccessFees"
  }
  /** Channel of newsfeed item */
  export enum NewsfeedChannel {
    Official = "official",
    Demanders = "demanders",
    AssociationAndDemander = "associationAndDemander",
    LegalProtectionAndDemander = "legalProtectionAndDemander",
    AssociationAndLawyer = "associationAndLawyer",
    LegalProtectionAndLawyer = "legalProtectionAndLawyer",
    AccountantAndDemander = "accountantAndDemander",
    AllDemanders = "allDemanders",
    Defenders = "defenders",
    Lawyers = "lawyers",
    DemanderBailiff = "demanderBailiff",
    ClerkAndDemanderLawyer = "clerkAndDemanderLawyer",
    ClerkAndDefenderLawyer = "clerkAndDefenderLawyer",
    ClerkAndLawyers = "clerkAndLawyers"
  }
  /** Type of newsfeed item */
  export enum NewsfeedCategory {
    Event = "event",
    DocumentAttached = "documentAttached",
    Waiting = "waiting",
    CallToAction = "callToAction",
    StartProsecution = "startProsecution",
    Counterproposal = "counterproposal",
    FormalNoticeSent = "formalNoticeSent",
    ComplaintSent = "complaintSent",
    ParticipantJoined = "participantJoined",
    Message = "message",
    Finished = "finished",
    Closed = "closed",
    Expired = "expired",
    AskDetails = "askDetails",
    StepChange = "stepChange",
    Amendment = "amendment",
    Reminder = "reminder",
    ContractSigned = "contractSigned",
    SupportOperation = "supportOperation",
    ChangeInvitationTarget = "changeInvitationTarget",
    ChangeDefender = "changeDefender",
    BailiffInvitation = "bailiffInvitation",
    CourtClerkInvited = "courtClerkInvited",
    InvitationRejection = "invitationRejection",
    Invitation = "invitation",
    ConciliationDateGiven = "conciliationDateGiven",
    FirstHearingDateGiven = "firstHearingDateGiven",
    HearingPostponed = "hearingPostponed",
    HearingDateDeleted = "hearingDateDeleted",
    HearingPast = "hearingPast",
    ConclusionsUploaded = "conclusionsUploaded",
    TransactionSet = "transactionSet",
    TransactionFailed = "transactionFailed",
    DecisionDateGiven = "decisionDateGiven",
    DecisionUploaded = "decisionUploaded",
    DecisionPast = "decisionPast",
    CourtFileNumberFilled = "courtFileNumberFilled",
    NotifyDisputeResolved = "notifyDisputeResolved",
    LetterNotDelivered = "letterNotDelivered",
    MediationExtended = "mediationExtended",
    SendLawyerLetter = "sendLawyerLetter",
    ConciliationReportUploaded = "conciliationReportUploaded"
  }
  /** A description of what is the negocition status of this claim */
  export enum ClaimStatus {
    Abandoned = "abandoned",
    Accepted = "accepted",
    Undecided = "undecided"
  }
  /** Currencies supported by Justice.cool */
  export enum Currency {
    Eur = "EUR"
  }
  /** The execution status of this claim */
  export enum ClaimExecutionStatus {
    Disagree = "disagree",
    Executed = "executed",
    ExecutedWaitingForJCool = "executedWaitingForJCool",
    NotEligible = "notEligible",
    Pending = "pending"
  }

  export enum AgreementType {
    Compensation = "compensation",
    Service = "service"
  }

  export enum AmendedClaimStatus {
    MissingInfo = "missingInfo",
    Modified = "modified",
    Unchanged = "unchanged",
    Created = "created",
    Deleted = "deleted"
  }
  /** Current of a fact */
  export enum DisputeFactStatus {
    Reviewing = "reviewing",
    Accepted = "accepted"
  }

  export enum AccessPolicyOverview {
    Public = "public",
    Custom = "custom",
    DemanderSide = "demanderSide",
    DefenderSide = "defenderSide",
    Private = "private"
  }

  export enum AccessPolicySource {
    Specific = "specific",
    Owner = "owner",
    Global = "global"
  }

  export enum DisputeFactAction {
    Agree = "agree",
    Contest = "contest",
    MyVersion = "myVersion"
  }

  export enum DisputeFactHistoryKind {
    MyVersion = "myVersion",
    Contest = "contest"
  }

  export enum TransactionProposalStatus {
    Proposition = "proposition",
    Acceptation = "acceptation"
  }

  export enum TransactionStatus {
    Disagree = "disagree",
    Pending = "pending",
    Sleeping = "sleeping",
    Executed = "executed",
    Stopped = "stopped"
  }
  /** Describes how an amendment behaves */
  export enum AmendmentBehaviour {
    ApplyOrThrow = "applyOrThrow",
    ApplyOrWait = "applyOrWait",
    Wait = "wait"
  }

  export enum FactExistsBehaviour {
    Throw = "throw",
    Replace = "replace",
    Append = "append",
    Skip = "skip"
  }

  export enum JcoolEntry {
    Mediation = "mediation",
    DemanderLawyerOnboarding = "demanderLawyerOnboarding",
    DemanderOrgOnboarding = "demanderOrgOnboarding"
  }

  export enum CustomVariableType {
    Date = "date",
    Datetime = "datetime",
    Duration = "duration",
    Document = "document",
    Double = "double"
  }
  /** List of languages supported by Justice.cool */
  export enum Language {
    Fr = "fr",
    En = "en"
  }

  export enum SimpleAccess {
    MeOnly = "meOnly",
    MySide = "mySide",
    Everybody = "everybody"
  }

  /** A date-time string at UTC, such as 2007-12-03T10:15:30Z, compliant with the `date-time` format outlined in section 5.6 of the RFC 3339 profile of the ISO 8601 standard for representation of dates and times using the Gregorian calendar. */
  export type DateTime = any;

  /** A string which value depends on Accept-Language request headers. */
  export type LocalizedString = any;

  /** The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  export type Json = any;

  // ====================================================
  // Scalars
  // ====================================================

  // ====================================================
  // Interfaces
  // ====================================================

  export interface IEntitySummary {
    id: string;

    type: EntityType;
    /** A link to visit this entity on justice.cool */
    link: string;
    /** Date at which this entity has been created */
    creationDate: DateTime;
    /** Shortcut to get the demander lawyer, if any lawyer has been assigned. */
    demanderLawyer?: Maybe<DisputeParticipant>;
    /** Shortcut to get the defender lawyer, if any lawyer has been assigned. */
    defenderLawyer?: Maybe<DisputeParticipant>;
    /** The side of the contact who makes the query. */
    mySide?: Maybe<DisputeSide>;
    /** The role of the contact who makes the query. */
    myRole?: Maybe<AccountRole>;
    /** Who am I, when interacting with this entity ? */
    me?: Maybe<DisputeParticipant>;
    /** Shortcut to get your opponent lawyer, if any lawyer has been assigned. */
    opponentLawyer?: Maybe<DisputeParticipant>;
    /** Shortcut to get the defender participant. */
    defender?: Maybe<DisputeParticipant>;
    /** A user-friendly description of the current dispute state */
    step?: Maybe<HumanReadableDescription>;
    /** (⚠ ADVANCED) When created on a namespace (on.justice.cool, for instance), this will be the ID of this namespace */
    namespace?: Maybe<string>;
    /** Your (optional) external reference provided for this dispute */
    externalId?: Maybe<string>;
    /** Justice.cool sometimes detects references from your CRM automatic replies. These are filled here (best effort detection) */
    externalReferences?: Maybe<(Maybe<string>)[]>;
  }

  export interface IEntity {
    id: string;

    type: EntityType;
    /** A link to visit this entity on justice.cool */
    link: string;
    /** Date at which this entity has been created */
    creationDate: DateTime;
    /** Shortcut to get the demander lawyer, if any lawyer has been assigned. */
    demanderLawyer?: Maybe<DisputeParticipant>;
    /** Shortcut to get the defender lawyer, if any lawyer has been assigned. */
    defenderLawyer?: Maybe<DisputeParticipant>;
    /** The side of the contact who makes the query. */
    mySide?: Maybe<DisputeSide>;
    /** The role of the contact who makes the query. */
    myRole?: Maybe<AccountRole>;
    /** Who am I, when interacting with this entity ? */
    me?: Maybe<DisputeParticipant>;
    /** Shortcut to get your opponent lawyer, if any lawyer has been assigned. */
    opponentLawyer?: Maybe<DisputeParticipant>;
    /** Shortcut to get the defender participant. */
    defender?: Maybe<DisputeParticipant>;
    /** A user-friendly description of the current dispute state */
    step?: Maybe<HumanReadableDescription>;
    /** (⚠ ADVANCED) When created on a namespace (on.justice.cool, for instance), this will be the ID of this namespace */
    namespace?: Maybe<string>;
    /** Your (optional) external reference provided for this dispute */
    externalId?: Maybe<string>;
    /** Justice.cool sometimes detects references from your CRM automatic replies. These are filled here (best effort detection) */
    externalReferences?: Maybe<(Maybe<string>)[]>;
    /** All newsfeed channels available to the logged-in user */
    newsfeedChannels?: Maybe<(Maybe<NewsfeedChannelConfig>)[]>;
    /** Newsfeed of this dispute (returns newest first) */
    newsfeed?: Maybe<(Maybe<Newsfeed>)[]>;
    /** List of all participants interacting with this entity */
    participants: DisputeParticipant[];
    /** List of all hashtags of the full text note attached to this dispute */
    tags: Hashtag[];
    /** Full text note. Hashtags in this note can be used to filter entities (and you will find them in the .tags property) */
    notes?: Maybe<EntityNotes[]>;
    /** List of documents attached to this dispute, that are not "facts" documents (ex: Mediation failure certificate, proof of postal deliveries, ...) */
    attachedDocuments: AttachedDocument[];
    /** Get a document that has been attached to this dispute, but that is not a "fact" document (ex: Mediation failure certificate, proof of postal deliveries, ...) Examples of well known document IDs: - mediationInvitation - lawyerFormalNotice - orgFormalNotice - complaint - complaintResponse - proofOfDeposit - servedSubpoena - subpoena - feeAgreement - subpoenaProject - petition - defenderConclusions - demanderConclusions - transaction - decision - caseDocuments - conciliationReport - custom */
    attachedDocument?: Maybe<AttachedDocument>;
  }

  /** Claim proposals common properties */
  export interface IClaimProposition {
    /** Which kind of proposal is this ? */
    status: ClaimStatus;
    /** Some (optional) comment about this proposal */
    comment?: Maybe<RichHtml>;
    /** Who made this proposal ? */
    by: DisputeSide;
    /** Indicative score associated with this proposal */
    score?: Maybe<Score>;
    /** Date at which this proposal has been posted */
    date: DateTime;
  }

  /** Some indicative information about the visibility policy of this object */
  export interface IAccessPolicySummary {
    /** Resource ID of this access. It can be used to gather more information or change the access policy of this object */
    id: string;
    /** Access type of this object */
    overview: AccessPolicyOverview;
    /** Who owns the access policy that has been used to compute access to this object */
    source: AccessPolicySource;
    /** Number of accesses given, if the given policy has countable items. This does not count the number of people that have access to this... */
    count?: Maybe<number>;
  }

  // ====================================================
  // Types
  // ====================================================

  export interface JcoolPublicQueries {
    /** All queries related to API hooks */
    hooks?: Maybe<HooksQueries>;
    /** Get all disputes that I have access to */
    disputes?: Maybe<(Maybe<DisputeSummary>)[]>;
    /** Get a dispute by its ID */
    dispute?: Maybe<Dispute>;
    /** Get all lawsuits that I have access to */
    lawsuits?: Maybe<(Maybe<LawsuitSummary>)[]>;
    /** Get a lawsuit by its ID */
    lawsuit?: Maybe<Lawsuit>;
  }

  /** All queries related to API hooks */
  export interface HooksQueries {
    /** Returns all the hook messages that have not been processed, ordered from older to newer. You might use this method to catchup hooks after a server shutdown. You will be responsible to call the "acknowledgeHooks" mutation on the hooks you processed. *IMPORTANT* This method only returns 50 elements. If it returned 50 elements, then there are probably more hooks pending: you must acknowledge these, then re-fetch this method. */
    list: HookMessage[];
  }

  /** A hook message that has not yet been processed */
  export interface HookMessage {
    /** An unique identifier for this message. Try to be omnipotent based on this ID */
    hookId: string;
    /** The hook type that must be processed */
    hookName: HookName;
    /** True if you configured this hook to be processed via polling */
    polling: boolean;
    /** Datetime at which this hook has been queued */
    time: DateTime;
    /** Human readable description of what this hook corresponds to */
    hookDescription?: Maybe<LocalizedString>;
    /** The number of times that justice.cool tried to perform this hook against your server */
    retryCount: number;
    /** Some hooks will provide data attached. This is it. */
    data?: Maybe<Json>;
    /** The entity id that is associated to this hook message */
    entityId?: Maybe<string>;
    /** The entity that is associated to this hook message */
    entity?: Maybe<EntitySummary>;
    /** True if this hook has been queued to be pushed to your servers. NB: If you specified the mode "polling" for this hook, then this property will be true until you acklowledge its processing. */
    queued?: Maybe<boolean>;
    /** Error that justice.cool has encountered when it tried to push this hook to your servers. */
    error?: Maybe<string>;
  }

  /** Summary of a dispute (pre-judiciary process) */
  export interface DisputeSummary extends IEntitySummary {
    id: string;

    type: EntityType;
    /** A link to visit this dispute on justice.cool */
    link: string;
    /** Date at which this entity has been created */
    creationDate: DateTime;
    /** Shortcut to get the demander lawyer, if any lawyer has been assigned. */
    demanderLawyer?: Maybe<DisputeParticipant>;
    /** Shortcut to get the defender lawyer, if any lawyer has been assigned. */
    defenderLawyer?: Maybe<DisputeParticipant>;
    /** The side of the contact who makes the query. */
    mySide?: Maybe<DisputeSide>;
    /** The role of the contact who makes the query. */
    myRole?: Maybe<AccountRole>;
    /** Who am I, when interacting with this entity ? */
    me?: Maybe<DisputeParticipant>;
    /** Shortcut to get your opponent lawyer, if any lawyer has been assigned. */
    opponentLawyer?: Maybe<DisputeParticipant>;
    /** Shortcut to get the defender participant. */
    defender?: Maybe<DisputeParticipant>;
    /** A user-friendly description of the current dispute state */
    step?: Maybe<HumanReadableDescription>;
    /** (⚠ ADVANCED) When created on a namespace (on.justice.cool, for instance), this will be the ID of this namespace */
    namespace?: Maybe<string>;
    /** Your (optional) external reference provided for this dispute */
    externalId?: Maybe<string>;
    /** Justice.cool sometimes detects references from your CRM automatic replies. These are filled here (best effort detection) */
    externalReferences?: Maybe<(Maybe<string>)[]>;
    /** Signature mode for this dispute (see DisputeCreationInput and justice.cool API documentation) */
    signatureMode?: Maybe<SignatureMode>;
    /** Justice.cool features chosen for this dispute (see DisputeCreationInput and justice.cool API documentation) */
    features?: Maybe<JCoolFeature[]>;
    /** (⚠ ADVANCED) When created a custom model, this will be the model used */
    model?: Maybe<string>;
    /** Claim eligibility update mode for this dispute (see DisputeCreationInput and justice.cool API documentation) */
    updateMode: UpdateMode;
    /** Deadline for the current step */
    countDownDate?: Maybe<DateTime>;
    /** Dispute score data */
    score?: Maybe<Score>;
    /** Matches a given filter */
    matchesFilter?: Maybe<boolean>;
    /** Litigation types have been involved in justice.cool scoring algorithm */
    eligibleLitigations?: Maybe<(Maybe<LocalizedString>)[]>;
    /** Actions that can be performed on this dispute, given its current state */
    availableActions: AvailableDisputeAction[];
    /** If justice.cool requires some information from the logged in user, then this will be a link pointing to a form to collect this information. */
    currentFormLink?: Maybe<string>;
    /** An email address that can be used to push new messages in this dispute */
    newsfeedEmail: string;
    /** Shortcut to get the "main" demander. This person represents all demanders. */
    mainDemander: DisputeParticipant;
    /** Shortcut to get demander participants */
    demanders: DisputeParticipant[];
    /** Tells how many demanders this dispute has. */
    demandersCount: number;
  }

  /** Someone who is involved in this dispute */
  export interface DisputeParticipant {
    /** Participant ID. Beware: this ID is NOT unique amongst multiple disputes (it can only be used to identify this participant in the current dispute) */
    id: string;
    /** Participant role in this dispute */
    role: AccountRole;
    /** Participant full name */
    name: string;
    /** Participant bank details */
    paymentDetails?: Maybe<BankDetails>;
    /** The name of the mandator, if exists. */
    mandator?: Maybe<string>;
    /** Opaque identifier that you can use as the safest way to group on the identity of this person/company accross disputes. ⚠ This identifier might change as your dispute evolves (for instance if the dispute has been transfered by your opponent to another legal entity) */
    identityHash: string;
    /** Some details about the participant, if you have access to them ⚠ This will be null if you dont have access to participant details */
    details?: Maybe<DisputeParticipantDetails>;
  }

  export interface BankDetails {
    iban?: Maybe<string>;

    bic?: Maybe<string>;
  }

  export interface PersonParticipantDetails {
    nationality?: Maybe<Country>;

    birthDate?: Maybe<DateTime>;

    cityOfBirth?: Maybe<string>;

    firstName: string;

    lastName: string;

    gender?: Maybe<Gender>;

    profession?: Maybe<LocalizedString>;
  }

  export interface Country {
    /** ISO 3166-1 alpha-3 country code (https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3) */
    id: string;
    /** Country name (ex: France) */
    name: LocalizedString;
    /** Describes the nationality/citizenship of this country (ex: French) */
    demonym: LocalizedString;
  }

  export interface CompanyParticipantDetails {
    country?: Maybe<Country>;

    legalIdentifier?: Maybe<string>;

    legalName?: Maybe<string>;
  }

  /** Formatted description of an item */
  export interface HumanReadableDescription {
    id?: Maybe<string>;

    title?: Maybe<LocalizedString>;
    /** An HTML detailed description */
    html?: Maybe<LocalizedString>;
    /** A category for this step (macro vision) */
    categoryId?: Maybe<string>;
    /** How long this usually lasts (indicative value, if applicable) */
    duration?: Maybe<LocalizedString>;
  }

  /** Describes a score */
  export interface Score {
    success?: Maybe<number>;

    reliability?: Maybe<number>;

    similarity?: Maybe<number>;
  }

  /** Describes an action that is available on a dispute selection */
  export interface AvailableDisputeAction {
    /** Action that is available */
    action: DisputeAction;
    /** Tells if this action can be grouped (in this case, you might want to execute it on a dispute group instead of executing it on a single dispute) */
    groupedBy?: Maybe<LocalizedString>;
  }

  /** Summary of a lawsuit */
  export interface LawsuitSummary extends IEntitySummary {
    id: string;

    type: EntityType;
    /** A link to visit this lawsuit on justice.cool */
    link: string;
    /** Date at which this entity has been created */
    creationDate: DateTime;
    /** Shortcut to get the demander lawyer, if any lawyer has been assigned. */
    demanderLawyer?: Maybe<DisputeParticipant>;
    /** Shortcut to get the defender lawyer, if any lawyer has been assigned. */
    defenderLawyer?: Maybe<DisputeParticipant>;
    /** The side of the contact who makes the query. */
    mySide?: Maybe<DisputeSide>;
    /** The role of the contact who makes the query. */
    myRole?: Maybe<AccountRole>;
    /** Who am I, when interacting with this entity ? */
    me?: Maybe<DisputeParticipant>;
    /** Shortcut to get your opponent lawyer, if any lawyer has been assigned. */
    opponentLawyer?: Maybe<DisputeParticipant>;
    /** Shortcut to get the defender participant. */
    defender?: Maybe<DisputeParticipant>;
    /** A user-friendly description of the current dispute state */
    step?: Maybe<HumanReadableDescription>;
    /** (⚠ ADVANCED) When created on a namespace (on.justice.cool, for instance), this will be the ID of this namespace */
    namespace?: Maybe<string>;
    /** Your (optional) external reference provided for this dispute */
    externalId?: Maybe<string>;
    /** Justice.cool sometimes detects references from your CRM automatic replies. These are filled here (best effort detection) */
    externalReferences?: Maybe<(Maybe<string>)[]>;
    /** The id of disputes that are part of this lawsuit */
    disputeIds?: Maybe<(Maybe<string>)[]>;
    /** The number of disputes that are part of this lawsuit */
    disputeCount: number;
    /** Actions that can be performed on this lawsuit, given its current state */
    availableActions: AvailableLawsuitAction[];
    /** The next court hearing date (if applicable) */
    hearingsCount?: Maybe<number>;
    /** The court file reference number (if applicable) */
    courtFileNumber?: Maybe<string>;
    /** The demander CARPA "numéro d'affaire" */
    demanderCarpaRef?: Maybe<string>;
    /** The defender CARPA "numéro d'affaire" */
    defenderCarpaRef?: Maybe<string>;
    /** Demander payment details (useful to refund/pay him) */
    demanderExecutionPaymentDetails?: Maybe<Json>;
    /** Defender payment details (useful to refund/pay him) */
    defenderExecutionPaymentDetails?: Maybe<Json>;
    /** The demander CARPA "numéro d'affaire" */
    demanderCarpaRib?: Maybe<string>;
    /** The defender CARPA "numéro d'affaire" */
    defenderCarpaRib?: Maybe<string>;
  }

  /** Describes an action that is available on a dispute selection */
  export interface AvailableLawsuitAction {
    /** Action that is available */
    action: LawsuitAction;
    /** Tells if this action can be grouped (in this case, you might want to execute it on a dispute group instead of executing it on a single dispute) */
    groupedBy?: Maybe<LocalizedString>;
  }

  export interface Dispute extends IEntity, IEntitySummary {
    id: string;

    type: EntityType;
    /** A link to visit this dispute on justice.cool */
    link: string;
    /** Date at which this entity has been created */
    creationDate: DateTime;
    /** Shortcut to get the demander lawyer, if any lawyer has been assigned. */
    demanderLawyer?: Maybe<DisputeParticipant>;
    /** Shortcut to get the defender lawyer, if any lawyer has been assigned. */
    defenderLawyer?: Maybe<DisputeParticipant>;
    /** The side of the contact who makes the query. */
    mySide?: Maybe<DisputeSide>;
    /** The role of the contact who makes the query. */
    myRole?: Maybe<AccountRole>;
    /** Who am I, when interacting with this entity ? */
    me?: Maybe<DisputeParticipant>;
    /** Shortcut to get your opponent lawyer, if any lawyer has been assigned. */
    opponentLawyer?: Maybe<DisputeParticipant>;
    /** Shortcut to get the defender participant. */
    defender?: Maybe<DisputeParticipant>;
    /** A user-friendly description of the current dispute state */
    step?: Maybe<HumanReadableDescription>;
    /** (⚠ ADVANCED) When created on a namespace (on.justice.cool, for instance), this will be the ID of this namespace */
    namespace?: Maybe<string>;
    /** Your (optional) external reference provided for this dispute */
    externalId?: Maybe<string>;
    /** Justice.cool sometimes detects references from your CRM automatic replies. These are filled here (best effort detection) */
    externalReferences?: Maybe<(Maybe<string>)[]>;
    /** All newsfeed channels available to the logged-in user */
    newsfeedChannels?: Maybe<(Maybe<NewsfeedChannelConfig>)[]>;
    /** Newsfeed of this dispute (returns newest first) */
    newsfeed?: Maybe<(Maybe<Newsfeed>)[]>;
    /** List of all participants interacting with this entity */
    participants: DisputeParticipant[];
    /** List of all hashtags of the full text note attached to this dispute */
    tags: Hashtag[];
    /** Full text note. Hashtags in this note can be used to filter entities (and you will find them in the .tags property) */
    notes?: Maybe<EntityNotes[]>;
    /** List of documents attached to this dispute, that are not "facts" documents (ex: Mediation failure certificate, proof of postal deliveries, ...) */
    attachedDocuments: AttachedDocument[];
    /** Get a document that has been attached to this dispute, but that is not a "fact" document (ex: Mediation failure certificate, proof of postal deliveries, ...) Examples of well known document IDs: - mediationInvitation - lawyerFormalNotice - orgFormalNotice - complaint - complaintResponse - proofOfDeposit - servedSubpoena - subpoena - feeAgreement - subpoenaProject - petition - defenderConclusions - demanderConclusions - transaction - decision - caseDocuments - conciliationReport - custom */
    attachedDocument?: Maybe<AttachedDocument>;
    /** Signature mode for this dispute (see DisputeCreationInput and justice.cool API documentation) */
    signatureMode?: Maybe<SignatureMode>;
    /** Justice.cool features chosen for this dispute (see DisputeCreationInput and justice.cool API documentation) */
    features?: Maybe<JCoolFeature[]>;
    /** (⚠ ADVANCED) When created a custom model, this will be the model used */
    model?: Maybe<string>;
    /** Claim eligibility update mode for this dispute (see DisputeCreationInput and justice.cool API documentation) */
    updateMode: UpdateMode;
    /** Deadline for the current step */
    countDownDate?: Maybe<DateTime>;
    /** Dispute score data */
    score?: Maybe<Score>;
    /** Matches a given filter */
    matchesFilter?: Maybe<boolean>;
    /** Litigation types have been involved in justice.cool scoring algorithm */
    eligibleLitigations?: Maybe<(Maybe<LocalizedString>)[]>;
    /** Actions that can be performed on this dispute, given its current state */
    availableActions: AvailableDisputeAction[];
    /** If justice.cool requires some information from the logged in user, then this will be a link pointing to a form to collect this information. */
    currentFormLink?: Maybe<string>;
    /** An email address that can be used to push new messages in this dispute */
    newsfeedEmail: string;
    /** Shortcut to get the "main" demander. This person represents all demanders. */
    mainDemander: DisputeParticipant;
    /** Shortcut to get demander participants */
    demanders: DisputeParticipant[];
    /** Tells how many demanders this dispute has. */
    demandersCount: number;
    /** Get a claim on this dispute */
    claim?: Maybe<Claim>;
    /** Every claim linked to this dispute. */
    claims?: Maybe<(Maybe<Claim>)[]>;
    /** Court decisions related to this dispute */
    decisions?: Maybe<(Maybe<CourtDecision>)[]>;
    /** The total amount to pay by the participant at this step */
    totalAmountToPay?: Maybe<number>;
    /** Get a fact amendment being applied in order to inspect it */
    amendment?: Maybe<FactsAmendment>;
    /** Lists the facts about a dispute. They are listed in the same order as they have been provided / answered in form. */
    facts: DisputeFact[];
    /** [DEV ONLY] If opponent has a waiting form, that will be it. */
    _DEV_opponentForm?: Maybe<Json>;
  }

  /** Describes a newsfeed channel for a given user */
  export interface NewsfeedChannelConfig {
    channel: NewsfeedChannel;

    name: LocalizedString;
    /** If readonly, this will be a string describing why this newsfeed is readonly */
    readonly?: Maybe<LocalizedString>;

    email: string;
    /** Roles who have access to this channel */
    visible?: Maybe<(Maybe<AccountRole>)[]>;
    /** Participants who have access to this channel */
    participants?: Maybe<(Maybe<ChannelParticipant>)[]>;
  }

  /** Someone who is involved in this channel */
  export interface ChannelParticipant {
    /** Infos about the participant in the entity */
    participant?: Maybe<DisputeParticipant>;

    readonly?: Maybe<boolean>;
  }

  /** A newsfeed item */
  export interface Newsfeed {
    /** The ID of this newsfeed event */
    id?: Maybe<string>;
    /** Newfeed category (tells you which kind of event this is) */
    category?: Maybe<NewsfeedCategory>;
    /** Newsfeed channel (tells you which "channel" this newsfeed is in). Channels are usually reserved to messages. */
    channel?: Maybe<NewsfeedChannel>;
    /** Data attached to this newsfeed (format depends on newsfeed type) */
    content?: Maybe<NewsfeedData>;
    /** An informative icon name corresponding to this newsfeed item type. The values it returns are subject to change, and you must not rely on it too much. */
    icon?: Maybe<string>;
    /** Date at which this event has occured */
    date: DateTime;
    /** Some info about the entities on which this newsfeed item is attached */
    entities?: Maybe<NewsfeedEntities>;
    /** True if this account marked it as processed */
    processed?: Maybe<boolean>;
    /** An user friendly HTML representation of this newsfeed */
    html?: Maybe<LocalizedString>;
    /** Tags you might have applied to this newsfeed item */
    tags?: Maybe<string[]>;
  }

  /** A message from someone (could have been posted via justice.cool, via email, ...) */
  export interface NewsfeedMessageData {
    /** The message body */
    body?: Maybe<RichHtml>;
    /** The message topic */
    topics?: Maybe<(Maybe<MessageTopic>)[]>;
    /** Who sent this message, if it is a message. */
    sender?: Maybe<MessageSender>;
  }

  /** Some HTML, translated in your prefered language, without scripts, fancy tags, nor css styles, and with consistent css classes */
  export interface RichHtml {
    /** Clean HTML representation of this message (without any scripts nor css styles, safe to display. See documentation for styling), translated in your prefered language */
    html: string;
    /** Full text representation of this message (DO NOT DISPLAY AS HTML WITHOUT ESCAPING IT !), translated in your prefered language */
    text: string;
    /** List of documents that might have been attached to this message */
    documents?: Maybe<(Maybe<Document>)[]>;
    /** If this message has been written in a language that is not your prefered language, then this will contain the original message (as written by its author) */
    original?: Maybe<RichHtmlOriginal>;
  }

  /** A document */
  export interface Document {
    /** An identifier for this document (its structure is justice.cool implementation details) */
    documentUri: string;
    /** A link where this document can be downloaded (temporary authenticated URL, only valid for several hours) */
    link: string;
  }

  export interface RichHtmlOriginal {
    /** Untranslated clean HTML representation of this message (without any scripts nor css styles, safe to display. See documentation for styling) */
    html: string;
    /** Untranslated full text representation of this message (DO NOT DISPLAY AS HTML WITHOUT ESCAPING IT !), translated in your prefered language */
    text: string;
    /** Language code in which this message was authored (ex: fr, en, es, ...) */
    language: string;
  }

  export interface MessageTopic {
    id?: Maybe<string>;
  }

  export interface MessageSender {
    id?: Maybe<string>;

    name?: Maybe<string>;

    role?: Maybe<AccountRole>;
  }

  /** When dispute has changed step */
  export interface NewsfeedStepChangeData {
    /** Previous step this dispute was on (null on the first occurence of this event) */
    previous?: Maybe<Step>;
    /** This dispute entered this step */
    step: Step;
  }

  export interface Step {
    /** ID of this step */
    id: string;
    /** Name of this step */
    name?: Maybe<LocalizedString>;
  }

  export interface NewsfeedSupportOperationData {
    message?: Maybe<string>;
  }

  /** When a contract has been signed */
  export interface NewsfeedContractSignedData {
    /** Name of the signer */
    signer?: Maybe<string[]>;
    /** ID of the contract */
    contractId?: Maybe<string>;
    /** Name of the contract */
    contractName?: Maybe<LocalizedString>;
  }

  export interface NewsfeedBailiffInvitationData {
    name?: Maybe<string>;
  }

  export interface NewsfeedClerkInvitationData {
    name?: Maybe<string>;
  }

  /** Details about someone who has rejected an invitation */
  export interface NewsfeedRejectionData {
    /** The message body */
    message?: Maybe<RichHtml>;
    /** Who sent this message, if it is a message. */
    rejected: RejectedRole;
  }

  export interface RejectedRole {
    name?: Maybe<string>;

    role: AccountRole;
  }

  export interface NewsfeedInvitationData {
    name?: Maybe<string>;

    role?: Maybe<string>;
  }

  export interface NoNewsfeedData {
    none?: Maybe<boolean>;
  }

  export interface NewsfeedEntities {
    /** Which type of entity are we talking about ? */
    type: EntityType;
    /** How many entities are we talking about (⚠ if count available) */
    count?: Maybe<number>;
    /** Entities group name */
    groupName?: Maybe<LocalizedString>;
    /** A link telling where you could navigate to know more about this item (if available) */
    linkFull?: Maybe<string>;
    /** A link telling where you could navigate to know more about this item (if available) */
    linkSlug?: Maybe<string>;
  }

  /** Some statistics about a hashtag */
  export interface Hashtag {
    /** The hashtag, lowercase */
    id: string;
    /** The hashtag, case senstive version */
    cased: string;
    /** The last time it was referenced */
    lastReference: DateTime;
    /** How many times this hashtag is been used */
    references: number;
  }

  export interface EntityNotes {
    html?: Maybe<string>;

    date?: Maybe<DateTime>;
  }

  /** A document attached to this dispute */
  export interface AttachedDocument {
    docId: string;
    /** An identifier for this document (its structure is justice.cool implementation details) */
    documentUri: string;
    /** Document name */
    name: LocalizedString;
    /** Document creation/upload date */
    date?: Maybe<DateTime>;
    /** A link where this document can be downloaded (temporary authenticated URL) */
    link: string;
  }

  /** A claim */
  export interface Claim {
    /** The unique ID of this claim */
    id: string;
    /** All the agreement proposals posted by both parties (newer first). */
    propositions?: Maybe<(Maybe<ClaimProposition>)[]>;
    /** The last proposal that has been posted */
    lastProposition?: Maybe<ClaimProposition>;
    /** The first agreement proposal that has been posted */
    initialProposition?: Maybe<ClaimProposition>;
    /** The last proposal that I posted */
    myLastProposition?: Maybe<ClaimProposition>;
    /** Name of this claim */
    name: LocalizedString;
    /** Negociation status of this claim (whether if both parties agreed on this) */
    status: ClaimStatus;
    /** If this claim typology is known to justice.cool, this will be its type identifier */
    typeId?: Maybe<string>;
    /** Execution status of the claim, given that this claim has been agreed by both parties (not really applicable if justice.cool is only used during the mediation phase) */
    executionStatus?: Maybe<ClaimExecutionStatus>;
    /** Agreement type */
    type: AgreementType;
    /** The dispute participant that this claim is a reparation to (only useful for multi-demander disputes) */
    participant?: Maybe<DisputeParticipant>;
  }

  export interface ClaimServiceProposition extends IClaimProposition {
    /** Which kind of proposal is this ? */
    status: ClaimStatus;
    /** Some (optional) comment about this proposal */
    comment?: Maybe<RichHtml>;
    /** Who made this proposal ? */
    by: DisputeSide;
    /** Indicative score associated with this proposal */
    score?: Maybe<Score>;
    /** Date at which this proposal has been posted */
    date: DateTime;
    /** Service proposal */
    service?: Maybe<ClaimService>;
  }

  /** Claim service description */
  export interface ClaimService {
    /** How much time (in days) is given to deliver the service, starting after dispute resolution */
    delay: number;
    /** Optional description/details of the service */
    description?: Maybe<string>;
  }

  export interface ClaimCompensationProposition extends IClaimProposition {
    /** Which kind of proposal is this ? */
    status: ClaimStatus;
    /** Some (optional) comment about this proposal */
    comment?: Maybe<RichHtml>;
    /** Who made this proposal ? */
    by: DisputeSide;
    /** Indicative score associated with this proposal */
    score?: Maybe<Score>;
    /** Date at which this proposal has been posted */
    date: DateTime;
    /** Financial proposal (i.e. amount to pay) */
    compensation?: Maybe<ClaimCompensation>;
  }

  /** Claim compensation (money). You can either provide a formulat to compute how much will be due (the amount will be updated based on the negociation of facts), or give a fixed amount. */
  export interface ClaimCompensation {
    /** A fixed amount for this compensation */
    amount: number;
    /** A formulat that can be used to compute this compensation from given variables (see documentation) */
    formula?: Maybe<string>;
    /** The corresponding curency */
    currency?: Maybe<Currency>;
  }

  export interface ClaimAbandonProposition extends IClaimProposition {
    /** Which kind of proposal is this ? */
    status: ClaimStatus;
    /** Some (optional) comment about this proposal */
    comment?: Maybe<RichHtml>;
    /** Who made this proposal ? */
    by: DisputeSide;
    /** Indicative score associated with this proposal */
    score?: Maybe<Score>;
    /** Date at which this proposal has been posted */
    date: DateTime;
  }

  /** A decision given by a court */
  export interface CourtDecision {
    /** Decision ID */
    id: string;
    /** Decision title */
    name: LocalizedString;
    /** ID of the entity */
    entity: string;

    accepted?: Maybe<boolean>;
    /** Name of the participant */
    participant?: Maybe<string>;

    notes?: Maybe<string>;

    decision: Json;

    setBy?: Maybe<string>;

    date?: Maybe<DateTime>;

    public?: Maybe<boolean>;
  }

  /** Describes the result of a fact modification */
  export interface FactsAmendment {
    /** Amendment ID. You will have to select in order to provide missing info if you did not use the 'applyOrThrow' behaviour (see 'amendFacts' mutation). */
    amendmentId: string;
    /** Equivalent of missingInfoFromMe.length > 0 */
    isMissingInfoFromMe: boolean;
    /** This piece of information is missing before Justice.cool can update claims & forward your review to your opponent. You must provide it. (see https://docs.justice.cool/#/known-variables) */
    missingInfoFromMe: MissingInfo[];
    /** Some info is missing, that must be provided by your opponent. This missing info blocks automatic update of claims. */
    missingOpponentInfo: boolean;
    /** A representation of how the claims are affected by this amendment */
    claimAmendments: AmendedClaim[];
  }

  /** Describes an info that is missing */
  export interface MissingInfo {
    /** Variable ID of the piece of info that is missing (see https://docs.justice.cool/#/known-variables) */
    variable: string;
    /** Fact reference of the previously answered value, if it has previously been answered */
    ref?: Maybe<string>;
    /** Variable name of the piece of info that is missing */
    name?: Maybe<LocalizedString>;
    /** If this missing info is about a specific participant ("personal" info), then this will be provided. It will be null otherwise. */
    for?: Maybe<DisputeParticipant>;
  }

  /** Describes how a claims has been amended due to a fact amendment */
  export interface AmendedClaim {
    status?: Maybe<AmendedClaimStatus>;
    /** The unique ID of this claim (null if this claim is new) */
    id?: Maybe<string>;
    /** Name of this claim */
    name: LocalizedString;
    /** Tells which participant this claim belong sto */
    participant: DisputeParticipant;
    /** Claim type id (see https://docs.justice.cool/#/known-variables?id=claim-types) */
    typeId?: Maybe<string>;
    /** The old version of this claim (null if status==created) */
    old?: Maybe<ClaimConcreteProposition>;
    /** The new version of this claim (null if status==deleted) */
    new?: Maybe<ClaimConcreteProposition>;
  }

  /** A fact about a dispute */
  export interface DisputeFact {
    /** Answer unique identifier */
    ref: string;
    /** Fact variable ID */
    variable?: Maybe<string>;
    /** Fact name (which is usually the variable name) */
    name?: Maybe<LocalizedString>;
    /** Question that has been or could have been asked to user to get this value */
    question?: Maybe<PublicQuestion>;
    /** Status of this fact, as of now */
    status?: Maybe<DisputeFactStatus>;
    /** The current value of this fact (in its serialized form) */
    value?: Maybe<Json>;
    /** Value type of this fact (advanced) */
    valueType?: Maybe<Json>;
    /** If you are the handler associated with this fact, you will have some info about its access here */
    access?: Maybe<AccessPolicySummary>;
    /** Describes which review action can be performed */
    possibleActions: DisputeFactAction[];
    /** If this fact is a participant-specific variable (ex: "age"), then this is the participant this fact is about */
    participant?: Maybe<DisputeParticipant>;
    /** During the mediation phase, both parties will start reviewing facts. Each party is given the chance to review it, comment it, bring some concreet proof of it, and give its own version of it until both agree on a definitive version. This property represents the history of this negociation (Newer versions first). NB: The latest version of this fact is also in this list (thus, history is never empty) */
    history?: Maybe<(Maybe<DisputeFactHistory>)[]>;
    /** Will be true if this fact has more than 1 version */
    hasHistory: boolean;
  }

  export interface PublicQuestion {
    /** Main question title */
    title: LocalizedString;
    /** Optional detailed instructions about this question */
    placeholder?: Maybe<LocalizedString>;
    /** Optional placeholder explanation */
    description?: Maybe<LocalizedString>;
  }

  /** Some indicative information about the visibility policy of this object */
  export interface AccessPolicySummary extends IAccessPolicySummary {
    /** Resource ID of this access. It can be used to gather more information or change the access policy of this object */
    id: string;
    /** Access type of this object */
    overview: AccessPolicyOverview;
    /** Who owns the access policy that has been used to compute access to this object */
    source: AccessPolicySource;
    /** Number of accesses given, if the given policy has countable items. This does not count the number of people that have access to this... */
    count?: Maybe<number>;
  }

  /** A negociation step around this fact */
  export interface DisputeFactHistory {
    /** The other partys's version of this fact (NB: will be null when type=contest) */
    value?: Maybe<Json>;
    /** Which kind of history is it ? */
    kind?: Maybe<DisputeFactHistoryKind>;
    /** Comment about this version */
    comment?: Maybe<RichHtml>;
    /** Date at which this version has been created */
    date?: Maybe<DateTime>;
    /** Whos version is that ? */
    by?: Maybe<DisputeSide>;
  }

  export interface Lawsuit extends IEntitySummary, IEntity {
    id: string;

    type: EntityType;
    /** A link to visit this lawsuit on justice.cool */
    link: string;
    /** Date at which this entity has been created */
    creationDate: DateTime;
    /** Shortcut to get the demander lawyer, if any lawyer has been assigned. */
    demanderLawyer?: Maybe<DisputeParticipant>;
    /** Shortcut to get the defender lawyer, if any lawyer has been assigned. */
    defenderLawyer?: Maybe<DisputeParticipant>;
    /** The side of the contact who makes the query. */
    mySide?: Maybe<DisputeSide>;
    /** The role of the contact who makes the query. */
    myRole?: Maybe<AccountRole>;
    /** Who am I, when interacting with this entity ? */
    me?: Maybe<DisputeParticipant>;
    /** Shortcut to get your opponent lawyer, if any lawyer has been assigned. */
    opponentLawyer?: Maybe<DisputeParticipant>;
    /** Shortcut to get the defender participant. */
    defender?: Maybe<DisputeParticipant>;
    /** A user-friendly description of the current dispute state */
    step?: Maybe<HumanReadableDescription>;
    /** (⚠ ADVANCED) When created on a namespace (on.justice.cool, for instance), this will be the ID of this namespace */
    namespace?: Maybe<string>;
    /** Your (optional) external reference provided for this dispute */
    externalId?: Maybe<string>;
    /** Justice.cool sometimes detects references from your CRM automatic replies. These are filled here (best effort detection) */
    externalReferences?: Maybe<(Maybe<string>)[]>;
    /** All newsfeed channels available to the logged-in user */
    newsfeedChannels?: Maybe<(Maybe<NewsfeedChannelConfig>)[]>;
    /** Newsfeed of this dispute (returns newest first) */
    newsfeed?: Maybe<(Maybe<Newsfeed>)[]>;
    /** List of all participants interacting with this entity */
    participants: DisputeParticipant[];
    /** List of all hashtags of the full text note attached to this dispute */
    tags: Hashtag[];
    /** Full text note. Hashtags in this note can be used to filter entities (and you will find them in the .tags property) */
    notes?: Maybe<EntityNotes[]>;
    /** List of documents attached to this dispute, that are not "facts" documents (ex: Mediation failure certificate, proof of postal deliveries, ...) */
    attachedDocuments: AttachedDocument[];
    /** Get a document that has been attached to this dispute, but that is not a "fact" document (ex: Mediation failure certificate, proof of postal deliveries, ...) Examples of well known document IDs: - mediationInvitation - lawyerFormalNotice - orgFormalNotice - complaint - complaintResponse - proofOfDeposit - servedSubpoena - subpoena - feeAgreement - subpoenaProject - petition - defenderConclusions - demanderConclusions - transaction - decision - caseDocuments - conciliationReport - custom */
    attachedDocument?: Maybe<AttachedDocument>;
    /** The id of disputes that are part of this lawsuit */
    disputeIds?: Maybe<(Maybe<string>)[]>;
    /** The number of disputes that are part of this lawsuit */
    disputeCount: number;
    /** Actions that can be performed on this lawsuit, given its current state */
    availableActions: AvailableLawsuitAction[];
    /** The next court hearing date (if applicable) */
    hearingsCount?: Maybe<number>;
    /** The court file reference number (if applicable) */
    courtFileNumber?: Maybe<string>;
    /** The demander CARPA "numéro d'affaire" */
    demanderCarpaRef?: Maybe<string>;
    /** The defender CARPA "numéro d'affaire" */
    defenderCarpaRef?: Maybe<string>;
    /** Demander payment details (useful to refund/pay him) */
    demanderExecutionPaymentDetails?: Maybe<Json>;
    /** Defender payment details (useful to refund/pay him) */
    defenderExecutionPaymentDetails?: Maybe<Json>;
    /** The demander CARPA "numéro d'affaire" */
    demanderCarpaRib?: Maybe<string>;
    /** The defender CARPA "numéro d'affaire" */
    defenderCarpaRib?: Maybe<string>;
    /** List of disputes attached to this lawsuit */
    disputes: DisputeSummary[];
    /** List of closed disputes attached to this lawsuit */
    closedDisputes?: Maybe<DisputeSummary[]>;
    /** Court decisions related to this dispute */
    decisions?: Maybe<(Maybe<CourtDecision>)[]>;
    /** Info about the current transaction */
    transaction?: Maybe<Transaction>;
  }

  export interface Transaction {
    id?: Maybe<string>;

    step?: Maybe<string>;

    lastProposition?: Maybe<TransactionProposition>;

    propositions?: Maybe<(Maybe<TransactionProposition>)[]>;

    status?: Maybe<TransactionStatus>;
  }

  export interface TransactionProposition {
    by?: Maybe<DisputeSide>;

    date?: Maybe<DateTime>;

    claims?: Maybe<(Maybe<DecisionClaim>)[]>;

    status?: Maybe<TransactionProposalStatus>;

    legalCosts?: Maybe<DisputeSide>;
  }

  export interface DecisionClaim {
    amount?: Maybe<number>;

    name?: Maybe<LocalizedString>;

    participant?: Maybe<string>;

    disputeId?: Maybe<string>;

    agreementType?: Maybe<AgreementType>;

    accepted?: Maybe<boolean>;
  }

  export interface JcoolPublicMutations {
    /** Mutates a dispute, or performs actions related to it */
    dispute: DisputeMutations;
    /** Create a dispute in justice.cool */
    createDispute?: Maybe<DisputeCreationResult>;
    /** Attach a dispute you have been invited to to your account */
    attachDispute?: Maybe<Dispute>;
    /** All mutations related to API hooks */
    hooks?: Maybe<HooksMutations>;
    /** Mutates a lawsuit, or performs actions related to it */
    lawsuit: LawsuitMutations;
  }

  /** All mutations related to a specific dispute */
  export interface DisputeMutations {
    /** Sends a public message in the dispute. */
    postMessage?: Maybe<Newsfeed>;
    /** Adds or edits some piece of information to a running fact amendment. WARNING: It must be your turn to review this fact. */
    resumeFactsAmendment?: Maybe<FactsAmendment>;
    /** Modify a fact ⚠ Only available when "modifyFacts" action is available on your dispute. ⚠ You must either specify "variable" or "ref", but not both. */
    setFact?: Maybe<DisputeFact>;
    /** When in mediation phase, you can accept/ammend current version of facts here. WARNING: All facts for which you did not specify your own version will be considered as accepted. Returns the facts that appears to be missing. See our documentation. WARNING: Most often, there will be no missing fact. But if there are, you MUST answer them (or ask a human to fill the pending form). */
    amendFacts?: Maybe<FactsAmendment>;
    /** When in mediation phase, you can accept current version of a claim here. WARNING: You must have reviewed all facts first ! (see 'amendFacts') */
    acceptClaims?: Maybe<boolean>;
    /** When in mediation phase, you can post a counter proposal to one ore more claims via this mutation. WARNING: You must have reviewed all facts first ! (see 'amendFacts') */
    counterProposeClaims?: Maybe<boolean>;
    /** When in mediation phase, you can "reject" a claim proposal via this mutation. This will ask your opponent if he would be OK to abandon this claim. If the last claim proposal was an offer, then this action will tell the opponent that you wish to abandon this claim (he will thus be invited either to refuse or make another proposal). If the last claim proposal was a refusal of your opponent, then this action will cause the mediation to fail. WARNING: You must have reviewed all facts first ! (see 'amendFacts') */
    rejectClaims?: Maybe<boolean>;
    /** Performs an action on this dispute from the API. The action must be available (check 'availableActions' on your dispute) */
    performAction?: Maybe<Json>;
    /** Check action data before performing the action */
    checkActionData?: Maybe<LocalizedString>;
  }

  /** Result of the creation */
  export interface DisputeCreationResult {
    /** The created dispute ID */
    id: string;
    /** If you created the dispute in "auto" mode, and we detect that some information is missing, this will be a form link to fill with missing information. */
    form?: Maybe<string>;
    /** If you created a dispute for someone else, send him this link to sign in Justice.cool. If null, it means that an user already exists with the email address provided. */
    accountCreationForm?: Maybe<string>;
    /** Some more detailed information about the created dispute */
    details?: Maybe<Dispute>;
    /** [WILL NOT APPEAR IN PRODUCTION ENVIRONMENT] Returns the invitation form that would be sent to your opponent (if your dispute is not missing information) */
    DEV_OPPONENT_FORM?: Maybe<string>;
  }

  /** All mutations related to API hooks */
  export interface HooksMutations {
    /** Mark a set of hooks as processed (useful if you use polling instead of plain hooks - not recommanded). Returns the number of hooks processed. */
    markProcessed?: Maybe<number>;
    /** Mark a set of hooks as errored. They will not be retried unless you call the "retry" mutation. */
    markErrored?: Maybe<number>;
    /** Retry set of hooks. Returns the number of hooks retried. */
    retry?: Maybe<number>;
    /** Forces to post the given hook to the given entity, with sample data */
    testHook: string;
  }

  /** All mutations related to a specific dispute */
  export interface LawsuitMutations {
    /** Sends a public message in the dispute. */
    postMessage?: Maybe<Newsfeed>;
    /** Performs an action on this dispute from the API. The action must be available (check 'availableActions' on your dispute) */
    performAction?: Maybe<Json>;
    /** Check action data before performing the action */
    checkActionData?: Maybe<LocalizedString>;
  }

  // ====================================================
  // Arguments
  // ====================================================

  export interface DisputesJcoolPublicQueriesArgs {
    /** Limit the number or returned results (max: 100) */
    limit?: Maybe<number>;
    /** Use this argument for pagination. Will only return disputes which are older than the given dispute */
    olderThanId?: Maybe<string>;
  }
  export interface DisputeJcoolPublicQueriesArgs {
    /** The ID of the dispute */
    id: string;
  }
  export interface LawsuitsJcoolPublicQueriesArgs {
    /** Limit the number or returned results (max: 100) */
    limit?: Maybe<number>;
    /** Use this argument for pagination. Will only return lawsuits which are older than the given lawsuit */
    olderThanId?: Maybe<string>;
  }
  export interface LawsuitJcoolPublicQueriesArgs {
    /** The ID of the lawsuit */
    id: string;
  }
  export interface ListHooksQueriesArgs {
    /** Tells which hooks to retreive (defaults to "polling") */
    filter?: HookFilter;
    /** Only retreive hook messages after this specific hook id (excluded). Use this for paging through hooks, for instance if you have specified a "polling" strategy for this hook. */
    after?: Maybe<string>;
    /** Only retreive those hook types. */
    hooks?: Maybe<HookName[]>;
    /** Only retreive hooks related to those disputes */
    disputes?: Maybe<(Maybe<string>)[]>;
    /** Retreives newer hooks first */
    newerFirst?: boolean;
  }
  export interface DemanderLawyerDisputeSummaryArgs {
    /** If true, returns only if the demander lawyer is a physical participant (not one with a pending invitation) */
    noInvitation?: Maybe<boolean>;
  }
  export interface DefenderLawyerDisputeSummaryArgs {
    /** If true, returns only if the defender lawyer is a physical participant (not one with a pending invitation) */
    noInvitation?: Maybe<boolean>;
  }
  export interface MatchesFilterDisputeSummaryArgs {
    filter?: Maybe<DisputeFilterInput>;
  }
  export interface DemanderLawyerLawsuitSummaryArgs {
    /** If true, returns only if the demander lawyer is a physical participant (not one with a pending invitation) */
    noInvitation?: Maybe<boolean>;
  }
  export interface DefenderLawyerLawsuitSummaryArgs {
    /** If true, returns only if the defender lawyer is a physical participant (not one with a pending invitation) */
    noInvitation?: Maybe<boolean>;
  }
  export interface DemanderLawyerDisputeArgs {
    /** If true, returns only if the demander lawyer is a physical participant (not one with a pending invitation) */
    noInvitation?: Maybe<boolean>;
  }
  export interface DefenderLawyerDisputeArgs {
    /** If true, returns only if the defender lawyer is a physical participant (not one with a pending invitation) */
    noInvitation?: Maybe<boolean>;
  }
  export interface NewsfeedDisputeArgs {
    /** How many results to return */
    limit?: number;
    /** Only return items before the given newsfeed item id - use this for pagination (excluded) */
    before?: Maybe<string>;
    /** Only get newsfeed from a specific channel */
    channel?: Maybe<NewsfeedChannel>;
    /** Only take those newsfeed categories */
    categories?: Maybe<(Maybe<NewsfeedCategory>)[]>;
  }
  export interface AttachedDocumentDisputeArgs {
    id: string;
  }
  export interface MatchesFilterDisputeArgs {
    filter?: Maybe<DisputeFilterInput>;
  }
  export interface ClaimDisputeArgs {
    id: string;
  }
  export interface ClaimsDisputeArgs {
    /** Only return claims with this decision */
    statusFilter?: Maybe<ClaimStatus[]>;
    /** Only return claims which have not this decision */
    notStatusFilter?: Maybe<ClaimStatus[]>;
    /** Only return claims with this execution status */
    executionStatusFilter?: Maybe<ClaimExecutionStatus[]>;
    /** Only return claims which have not this execution status */
    notExecutionStatusFilter?: Maybe<ClaimExecutionStatus[]>;
    /** Only return claims that are awaiting my review */
    onlyWaitingMyReview?: Maybe<boolean>;
  }
  export interface AmendmentDisputeArgs {
    /** The amendement being applied */
    amendmentId: string;
  }
  export interface FactsDisputeArgs {
    /** Only return facts that have those statuses */
    statusFilter?: Maybe<(Maybe<DisputeFactStatus>)[]>;
    /** If true, will only get facts that are waiting for your review */
    onlyWaitingMyReview?: Maybe<boolean>;
    /** If true, will only get facts on which you have found an agreement */
    onlyAccepted?: Maybe<boolean>;
    /** Only get facts that can be amended/discussed/negociated with your opponent */
    onlyAmendable?: Maybe<boolean>;
    /** Filter types of answers */
    types?: Maybe<(Maybe<Json>)[]>;
    /** Filter types of answers */
    notTypes?: Maybe<(Maybe<Json>)[]>;
    /** Only answers which have this fact ID */
    factIds?: Maybe<(Maybe<string>)[]>;
    /** Only answers which have does not have this fact ID */
    notFactIds?: Maybe<(Maybe<string>)[]>;
    /** Get only those fact references */
    refs?: Maybe<(Maybe<string>)[]>;
    /** Do not get those fact references */
    notRefs?: Maybe<(Maybe<string>)[]>;
  }
  export interface OriginalRichHtmlArgs {
    /** If true, then this will never be null, even when the message is in your prefered language. */
    always?: boolean;
  }
  export interface HistoryDisputeFactArgs {
    /** How many history items should be retreived */
    limit?: Maybe<number>;
  }
  export interface DemanderLawyerLawsuitArgs {
    /** If true, returns only if the demander lawyer is a physical participant (not one with a pending invitation) */
    noInvitation?: Maybe<boolean>;
  }
  export interface DefenderLawyerLawsuitArgs {
    /** If true, returns only if the defender lawyer is a physical participant (not one with a pending invitation) */
    noInvitation?: Maybe<boolean>;
  }
  export interface NewsfeedLawsuitArgs {
    /** How many results to return */
    limit?: number;
    /** Only return items before the given newsfeed item id - use this for pagination (excluded) */
    before?: Maybe<string>;
    /** Only get newsfeed from a specific channel */
    channel?: Maybe<NewsfeedChannel>;
    /** Only take those newsfeed categories */
    categories?: Maybe<(Maybe<NewsfeedCategory>)[]>;
  }
  export interface AttachedDocumentLawsuitArgs {
    id: string;
  }
  export interface DisputeJcoolPublicMutationsArgs {
    id: string;
  }
  export interface CreateDisputeJcoolPublicMutationsArgs {
    /** Data necessary to create a new dispute */
    data?: Maybe<DisputeCreationInput>;
  }
  export interface AttachDisputeJcoolPublicMutationsArgs {
    /** The dispute ID you have been invited to (if you use this, you MUST provide invitationKey) */
    id?: Maybe<string>;
    /** The dispute invitation key you should have received (to use with disputeId) */
    invitationKey?: Maybe<string>;
    /** Invitation link you have received (no need to specifiy invitationKey nor dispute ID) */
    link?: Maybe<string>;
  }
  export interface LawsuitJcoolPublicMutationsArgs {
    id: string;
  }
  export interface PostMessageDisputeMutationsArgs {
    /** Message text content (incompatible with "html" input argument) */
    text?: Maybe<string>;
    /** Message html content (incompatible with "text" input argument) */
    html?: Maybe<RichHtmlInput>;
    /** Which channel to post in (see "newsfeedChannels" property of disputes to know which channels are available) */
    channel: NewsfeedChannel;
  }
  export interface ResumeFactsAmendmentDisputeMutationsArgs {
    /** The pending amendment ID */
    amendmentId: string;
    /** The fact to add or edit */
    facts: (Maybe<FactChangeInput>)[];
    /** Behaviour to adopt */
    behaviour: AmendmentBehaviour;
  }
  export interface SetFactDisputeMutationsArgs {
    /** The variable ID you want to set. ⚠ Not compatible with the "ref" argument */
    variable?: Maybe<IdPidInput>;
    /** The answer reference you want to set. ⚠ Not compatible with the "variable" argument. */
    ref?: Maybe<string>;
    /** The serialized fact value you want to set */
    value?: Maybe<Json>;
    /** Tells how already existing values will be handled */
    existing?: FactExistsBehaviour;
  }
  export interface AmendFactsDisputeMutationsArgs {
    /** The facts to be amended */
    reviews: FactAmendmentInput[];
    /** The fact to add */
    newFacts?: Maybe<FactChangeInput[]>;
    /** Behaviour to adopt */
    behaviour: AmendmentBehaviour;
  }
  export interface AcceptClaimsDisputeMutationsArgs {
    /** The facts to be amended with your own version */
    claims: string[];
  }
  export interface CounterProposeClaimsDisputeMutationsArgs {
    /** You counter-proposition(s) */
    propositions: ClaimCounterPropositionInput[];
  }
  export interface RejectClaimsDisputeMutationsArgs {
    /** The claim IDs for which you wish refuse last proposal. */
    reject: ClaimRejectionInput[];
  }
  export interface PerformActionDisputeMutationsArgs {
    /** The action to process */
    action: DisputeAction;
    /** Data attached to this action, if this actions requires data (see corresponding action documentation) */
    data?: Maybe<Json>;
    /** If true, it will not wait workflow processing before returning (meaning that the query will be processed faster, but with an eventual consistency guarantee) */
    noWait?: Maybe<boolean>;
  }
  export interface CheckActionDataDisputeMutationsArgs {
    /** The action to process */
    action: DisputeAction;
    /** Data attached to this action, if this actions requires data (see corresponding action documentation) */
    data?: Maybe<Json>;
  }
  export interface MarkProcessedHooksMutationsArgs {
    /** The hook IDs to mark as processed */
    hookIds: string[];
  }
  export interface MarkErroredHooksMutationsArgs {
    /** The hook IDs to mark as errored */
    hooks: HookErrorInput[];
  }
  export interface RetryHooksMutationsArgs {
    /** The hook IDs to mark as processed */
    hookIds: string[];
  }
  export interface TestHookHooksMutationsArgs {
    /** The hook to test */
    hook: HookName;
    /** The entity to test your hook on */
    onEntity: string;
  }
  export interface PostMessageLawsuitMutationsArgs {
    /** Message text content (incompatible with "html" input argument) */
    text?: Maybe<string>;
    /** Message html content (incompatible with "text" input argument) */
    html?: Maybe<RichHtmlInput>;
    /** Which channel to post in (see "newsfeedChannels" property of disputes to know which channels are available) */
    channel: NewsfeedChannel;
  }
  export interface PerformActionLawsuitMutationsArgs {
    /** The action to process */
    action: LawsuitAction;
    /** Data attached to this action, if this actions requires data (see corresponding action documentation) */
    data?: Maybe<Json>;
    /** If true, it will not wait workflow processing before returning (meaning that the query will be processed faster, but with an eventual consistency guarantee) */
    noWait?: Maybe<boolean>;
  }
  export interface CheckActionDataLawsuitMutationsArgs {
    /** The action to process */
    action: LawsuitAction;
    /** Data attached to this action, if this actions requires data (see corresponding action documentation) */
    data?: Maybe<Json>;
  }

  // ====================================================
  // Unions
  // ====================================================

  export type EntitySummary = DisputeSummary | LawsuitSummary;

  export type DisputeParticipantDetails =
    | PersonParticipantDetails
    | CompanyParticipantDetails;

  /** All types of newsfeed data you will encounter */
  export type NewsfeedData =
    | NewsfeedMessageData
    | NewsfeedStepChangeData
    | NewsfeedSupportOperationData
    | NewsfeedContractSignedData
    | NewsfeedBailiffInvitationData
    | NewsfeedClerkInvitationData
    | NewsfeedRejectionData
    | NewsfeedInvitationData
    | NoNewsfeedData;

  /** All types of claim proposals */
  export type ClaimProposition =
    | ClaimServiceProposition
    | ClaimCompensationProposition
    | ClaimAbandonProposition;

  /** All types of actual claim proposals */
  export type ClaimConcreteProposition =
    | ClaimServiceProposition
    | ClaimCompensationProposition;
}
