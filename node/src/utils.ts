export function assertUnreachable(x: never): Error {
    return new Error('Didn\'t expect to get at case ' + JSON.stringify(x));
}
