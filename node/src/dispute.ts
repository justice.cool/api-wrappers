import { IGqlClient, DisputeSimpleDetails, Message, Fact, Claim } from './services';
import gql from 'graphql-tag';
import { jcoolSchema } from './gql-types';
import { HtmlFragment } from './fragments';


export class DisputeManager {

    constructor(public readonly id: string, private client: IGqlClient) {

    }

    /** Get some details about this dispute */
    async getDetails(): Promise<DisputeSimpleDetails> {
        // get dispute details
        // nb: see DisputeSimpleDetails type def before modifying this query
        const { dispute } = await this.client.get(gql`query DisputeDetails($id: String!) {
            dispute(id: $id) {
                id
                externalId
                currentFormLink
                newsfeedEmail
                link
                step {
                    categoryId
                    duration
                    id
                    title
                }
                myRole
                creationDate
                participants {
                    id
                    name
                    role
                }
                countDownDate
            }
        }`, { id: this.id });

        // parse dates
        dispute.creationDate = new Date(dispute.creationDate);
        dispute.countDownDate = dispute.countDownDate && new Date(dispute.countDownDate);
        return dispute;
    }

    /** Fetch last messages posted in this dispute */
    async getMessages(): Promise<Message[]> {
        const { dispute } = await this.client.get(gql`query FetchNews($id: String!) {
            dispute(id: $id) {
                newsfeed (categories: [message]) {
                    category
                    channel
                    date
                    content {
                        ...on NewsfeedMessageData {
                            body {
                                ...HtmlFragment
                            }
                            sender {
                                id
                                name
                                role
                            }
                        }
                    }
                }
            }
        } ${HtmlFragment}`, { id: this.id });

        // parse dates
        return dispute.newsfeed.map(x => ({
            ...x,
            date: new Date(x.date),
            content: <jcoolSchema.NewsfeedMessageData>x.content
        }));
    }


    /** Fetch facts about this dispute */
    async getFacts(filter?: { onlyWaitingMyReview?: true }): Promise<Fact[]> {
        filter = filter || {};
        const { dispute } = await this.client.get(gql`query FetchFacts($id: String!, $revOnly: Boolean!) {
            dispute(id: $id) {
                facts(onlyWaitingMyReview: $revOnly) {
                    ref
                    name
                    participant {
                        id
                        name
                    }
                    value
                    valueType
                    status
                }
            }
        }`, {
            id: this.id,
            revOnly: filter.onlyWaitingMyReview || false,
        });

        return dispute.facts;
    }

    /** Fetch claims */
    async getClaims(options?: { onlyWaitingMyReview?: boolean; statusFilter?: jcoolSchema.ClaimStatus[] }): Promise<Claim[]> {
        options = options || {};

        // fetch claims
        const { dispute } = await this.client.get(gql`query FetchClaims($id: String!, $onlyRev: Boolean!, $status: [ClaimStatus!]) {
            dispute(id: $id) {
                claims (statusFilter: $status, onlyWaitingMyReview: $onlyRev) {
                    id
                    status
                    name
                    type
                    participant {
                        id
                        name
                    }
                    myLastProposition {
                        ...Prop
                    }
                    initialProposition {
                        ...Prop
                    }
                    lastProposition {
                        ... Prop
                    }
                }
            }
        }

        fragment Prop on ClaimProposition {
            ...on IClaimProposition {
                by
                comment { ... HtmlFragment }
                date
                status
            }
            ...on ClaimServiceProposition { service { delay } }
            ...on ClaimCompensationProposition { compensation { amount currency } }
        }
        ${HtmlFragment}
        `, {
            id: this.id,
            status: options.statusFilter || [],
            onlyRev: options.onlyWaitingMyReview || false,
        });

        // parse dates
        dispute.claims.forEach(c => {
            [...c.propositions || []
                , c.initialProposition
                , c.lastProposition
                , c.myLastProposition]
                .filter(x => !!x)
                .forEach(p => p.date = p.date && new Date(p.date));
        });

        return dispute.claims;
    }


    /**
     * Push your facts corrections. All facts that are not mentioned in corrections will be considered as accepted.
     *  */
    async pushFactCorrections(facts: jcoolSchema.FactAmendmentInput[]) {
        await this.client.mutate(gql`mutation AmendFacts($id: String!, $facts: [FactAmendmentInput!]!) {
            dispute(id: $id) {
                amendFacts(reviews: $facts, behaviour: applyOrThrow) {
                    amendmentId
                }
            }
        }`, {
            id: this.id,
            facts,
        });
    }

    /** Accept a claim decision (will fail if it is not your turn to do so, or if some facts need review) */
    async acceptClaims(claimIds: string[]) {
        await this.client.mutate(gql`mutation AcceptClaims($id: String!, $claims: [String!]!) {
            dispute(id: $id) {
                acceptClaims(claims: $claims)
            }
        }`, {
            id: this.id,
            claims: claimIds,
        });
    }

    /** Rejects a claim decision (will fail if it is not your turn to do so, or if some facts need review) */
    async rejectClaims(rejections: jcoolSchema.ClaimRejectionInput[]) {
        await this.client.mutate(gql`mutation RejectClaims($id: String!, $claims: [ClaimRejectionInput!]!) {
            dispute(id: $id) {
                rejectClaims(reject: $claims)
            }
        }`, {
            id: this.id,
            claims: rejections,
        });
    }

    /** Posts claim counter propositions (will fail if it is not your turn to do so, or if some facts need review) */
    async postClaimCounterPropositions(propositions: jcoolSchema.ClaimCounterPropositionInput[]) {
        await this.client.mutate(gql`mutation PostClaimCounterprop($id: String!, $claims: [ClaimCounterPropositionInput!]!) {
            dispute(id: $id) {
                counterProposeClaims(propositions: $claims)
            }
        }`, {
            id: this.id,
            claims: propositions,
        });
    }

    /** Posts a message to this dispute */
    async postMessage(message: string | jcoolSchema.RichHtmlInput) {
        await this.client.mutate(gql`mutation PostMessage($id: String!, $html: RichHtmlInput, $text: String) {
            dispute(id: $id) {
                postMessage (html: $html, text: $text) {
                    id
                }
            }
        }`, {
            id: this.id,
            text: typeof message === 'string' ? message : null,
            html: typeof message === 'string' ? null : message,
        });
    }

    /** Set this dispute as resolved (only works if you are demander in this dispute) */
    async resolveDispute() {
        await this.client.mutate(gql`mutation ResolveDispute($id: String!) {
            dispute(id: $id) {
                notifyEvent(event: disputeResolved)
            }
        }`, {
            id: this.id,
        })
    }
}