import gql from 'graphql-tag';

/** Selects everything in an HTML message */
export const HtmlFragment = gql`fragment HtmlFragment on RichHtml {
    html
    text
    documents {
        documentUri
        link
    }
    original {
        html
        text
        language
    }
}
`