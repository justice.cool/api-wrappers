import * as fs from 'fs';
import { IHooks, JCoolApi, Env } from '../src';


// parse local credentials
if (!fs.existsSync('credentials.json')) {
    console.error(`Please generate an API key on our staging environment (see https://docs.justice.cool/ ), and store it in credentials.json, following this format:
    {
        "key": "put your api key here"
    }`)
    process.exit(1);
}

const credentials = JSON.parse(fs.readFileSync('credentials.json', 'utf8'));
if (typeof credentials.key !== 'string') {
    console.error(`Invalid credentials.json format, it should be something like:
    {
        "key": "put your api key here"
    }`)
    process.exit(1);
}

export function connect(hooks: (x: JCoolApi) => IHooks) {
    const api = new JCoolApi(credentials.key, hooks, credentials.endpoint || Env.staging);

    // start polling events from justice.cool
    api.hooks.startSimplePolling();

    console.log('Waiting for events from Justice.cool...');
    return api;
}