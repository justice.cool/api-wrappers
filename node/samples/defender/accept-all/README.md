
# What is it ?

If you were a defender willing to accept everything in incoming mediations (which is very unlikely, but this is for the sake of this example), this is how you could implement it.


# How to test it ?

1) Using vscode, just select the `[node] [defender] Accept all` debug configuration, then hit F5.
2) Create a mediation against yourself using [justice.cool test tools](https://app.staging.justice.cool/dev/test-tools)
3) The `newDispute()` method should be called, and the created mediation should be automatically accepted.