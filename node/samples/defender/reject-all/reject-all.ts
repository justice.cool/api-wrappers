
import { connect } from '../../connect';
// you would import this from '@justice.cool/api-wrapper'
import { IHooks, HookPostBody, HookInvitationData, JCoolApi, jcoolSchema } from '../../../src';

export class Hooks implements IHooks {

    constructor(private api: JCoolApi) {
    }

    /**
     * The method that will be called when there is an incoming dispute
     */
    async newDispute(dispute: HookPostBody, inviation: HookInvitationData) {

        // we've received an invitation to a mediation
        const d = this.api.getDispute(dispute.disputeId);

        // === no fact correction ===
        // => we're just accepting all the facts that our opponent has given
        await d.pushFactCorrections([]);

        /* => ABOVE CALL IS EQUIVALENT TO QUERY:
        mutation postFactReviews($id: String!) {
            dispute(id: $id) {
                amendFacts(facts: [])
            }
        }
         */


        // === get all claim IDs ===
        const claims = await d.getClaims();

        /*  => ABOVE CALL IS EQUIVALENT TO QUERY:
        query getClaimIds($id: String!) {
            dispute(id: $id) {
                claims {
                    id
                }
            }
        }
         */


        // === accept claims ===
        await d.rejectClaims(claims.map<jcoolSchema.ClaimRejectionInput>(x => ({
            claimId: x.id,
        })));

        /* => ABOVE CALL IS EQUIVALENT TO QUERY:
        mutation acceptClaims($id: String!, $data: [ClaimRejectionInput!]!) {
            dispute(id: $id) {
                rejectClaims(claims: $data)
            }
        }
         */
    }
}

// connect to justice.cool api
connect(x => new Hooks(x));
