
# What is it ?

If you were a defender, and you would like to reject a mediation, one solution could be to accept the defender facts, and then reject all its claims. This sample explains how to implement this.


# How to test it ?

1) Using vscode, just select the `[node] [defender] Reject all` debug configuration, then hit F5.
2) Create a mediation against yourself using [justice.cool test tools](https://app.staging.justice.cool/dev/test-tools)
3) The `newDispute()` method should be called, and the created mediation should be automatically rejected.